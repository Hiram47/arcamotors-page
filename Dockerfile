FROM node:20-alpine3.17

WORKDIR /app

COPY package.json .

RUN npm install

COPY /src* .

EXPOSE 3999

CMD [ "npm","run","dev" ]