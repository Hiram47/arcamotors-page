import express from 'express'
import morgan from 'morgan';
import cookieParser from 'cookie-parser'

import authRoutes from './routes/auth.routes.js'
import taskRoutes from './routes/tasks.routes.js'
import ticketRoutes from './routes/ticket.routes.js'
import cors from 'cors'

const app = express()

app.use(cors({
    origin:['http://localhost:7800','http://10.11.60.124:7800','http://localhost:9099','http://10.11.60.124:9099'],
    credentials:true
}))
app.use(morgan('dev'))
app.use(express.json())
app.use(cookieParser())

app.use('/api',authRoutes)
app.use('/api',taskRoutes)
app.use('/api',ticketRoutes)

export default app;