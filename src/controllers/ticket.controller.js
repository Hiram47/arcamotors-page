import { request } from 'express';
import {connect} from '../db.js'

export const getTicketByUser= async(req,res)=>{
    try {

        let id=req.params.id;
        const db= await connect();

        //query helps to search lower an upper texts
        const [result] = await db.query(`SELECT id,id_vehiculo,venta2,usuario_modifico, enganche, meses,interes,mensualidad,pagos,fecha_registro,(venta2-enganche)AS diferencia,(venta2-enganche)/ meses AS pagonointeres FROM pagos where id_vehiculo= ? ORDER BY id desc`,[id])
        console.log(result);
        
        res.json(result)
        
    } catch (error) {
        
        console.log(error);
        
    }


}

// new get row to pay
export const getRecordToPay= async(req,res)=>{
    try {

        let id=req.params.id;
        const db= await connect();

        //query helps to search lower an upper texts
        const [result] = await db.query(`SELECT id,id_vehiculo,venta2,usuario_modifico, enganche, meses,interes,mensualidad,pagos,fecha_registro,fecha_mensualidad,(venta2-enganche)AS diferencia,(venta2-enganche)/ meses AS pagonointeres FROM pagos where id= ? ORDER BY id desc`,[id])
        console.log(result[0]);
        
        res.json(result[0])
        
    } catch (error) {
        
        console.log(error);
        
    }


}
// new update 
export const updateCobro= async(req, res)=>{
    try {
        let id=req.params.id;
        const pagos=req.body.pagos
        const db= await connect();
        const [result] = await  db.query(`UPDATE pagos SET pagos=?,fecha_registro=NOW() WHERE id=? `,[id,pagos])
        console.log(result);
        console.log('pagos',req.body.pagos);
        console.log('id',id);
        res.json({message:'Pago realizado correctamente'})
        
    } catch (error) {
        console.log(error);
        
    }
}


export const saveTicket= async(req,res)=>{

    try {
        
        const db = await connect()
        console.log(req.user,'noe');
        let id_usuario=req.user.id
    
        const result = await db.query("INSERT into pagos set ?",[req.body])    
        console.log(result);

        res.status(200).json('ticket was saved');   

    } catch (error) {

        console.log(error);
        
    }


}

export const deleteTicket= async(req,res)=>{
    try {
        const db = await connect()
        let id= req.params.id
        console.log(id);
        const result = await db.query(`DELETE FROM pagos WHERE id =?`,[id] )
        console.log(result);
        res.status(200).json('ticket was deleted');  
        
    } catch (error) {
        console.log(error);
        
    }
}

export const reporteByVehiculo = async (req,res)=>{
    try {
        const db=await connect()
        let id= req.params.id

        const [result] = await db.query('SELECT * FROM combustible where id_vehiculo =? and fecha BETWEEN ? and ?',[req.body.id_vehiculo,req.body.inicial,req.body.final])
        console.log(result);
        
        res.json(result)
        
    } catch (error) {
        console.log(error);
        
    }
}

export const puestoReporte = async (req,res)=>{
    try {
        const db=await connect()
        let id= req.params.id

        const [result] = await db.query('SELECT * FROM puesto',)
        console.log(result[0]);
        
        res.json(result[0])
        
    } catch (error) {
        console.log(error);
        
    }
}

export const reporteAllCars = async (req,res) =>{
    try {
        const db= await connect()

        // const [result] = await db.query("SELECT c.id_vehiculo, CONCAT_WS(' ',v.marca,v.submarca,v.modelo,v.color,v.placas,v.serie) AS carro,SUM(c.litros) AS litros, SUM(c.costo) AS costos , SUM(c.litros)*SUM(c.costo) AS total FROM combustible c INNER JOIN vehiculo v ON v.id=c.id_vehiculo WHERE  fecha BETWEEN ? AND ? GROUP BY c.id_vehiculo",[req.body.inicial,req.body.final])
        const [result] = await db.query("SELECT c.id_vehiculo,CONCAT_WS(' ',v.marca,v.submarca,v.modelo,v.color,v.placas,v.serie) AS carro,sum(c.litros) AS litros, SUM(c.costo) AS costos FROM combustible c INNER JOIN vehiculo v ON v.id=c.id_vehiculo WHERE  fecha BETWEEN ? AND ? GROUP BY c.id_vehiculo",[req.body.inicial,req.body.final])

        console.log(result);

        res.json(result)
        
    } catch (error) {
        console.log(error);
        
    }
}

// next month

export const getnextmonth= async(req,res)=>{
    try {
        const db= await connect()

        // const [result] = await db.query("SELECT c.id_vehiculo, CONCAT_WS(' ',v.marca,v.submarca,v.modelo,v.color,v.placas,v.serie) AS carro,SUM(c.litros) AS litros, SUM(c.costo) AS costos , SUM(c.litros)*SUM(c.costo) AS total FROM combustible c INNER JOIN vehiculo v ON v.id=c.id_vehiculo WHERE  fecha BETWEEN ? AND ? GROUP BY c.id_vehiculo",[req.body.inicial,req.body.final])
        // const [result] = await db.query("SELECT  DATE(DATE_ADD(?, INTERVAL 31 * ? DAY)) AS NEXT_MONTH",[req.body.inicial,req.body.pagos])
        const [result] = await db.query("SELECT  DATE(DATE_ADD(?, INTERVAL  ? MONTH)) AS NEXT_MONTH",[req.body.inicial,req.body.pagos])

        console.log(result[0]);

        res.json(result[0])
        
    } catch (error) {
        
    }
}

// interes por dia
export const getinteresbyDay= async(req,res)=>{
    try {
        const db= await connect()

        // const [result] = await db.query("SELECT c.id_vehiculo, CONCAT_WS(' ',v.marca,v.submarca,v.modelo,v.color,v.placas,v.serie) AS carro,SUM(c.litros) AS litros, SUM(c.costo) AS costos , SUM(c.litros)*SUM(c.costo) AS total FROM combustible c INNER JOIN vehiculo v ON v.id=c.id_vehiculo WHERE  fecha BETWEEN ? AND ? GROUP BY c.id_vehiculo",[req.body.inicial,req.body.final])
        const [result] = await db.query("SELECT TIMESTAMPDIFF(DAY,?, NOW() ) AS dias_transcurridos",[req.body.inicial])

        console.log(result[0]);

        res.json(result[0])
        
    } catch (error) {
        console.log(error);
        
    }
}

export const getReportMonthly= async(req,res)=>{
    try {
        const db= await connect()

        const [result]= await db.query('SELECT * FROM bitacora_pagos WHERE id_pago= ? AND id_vehiculo=? ORDER BY pago desc;',[req.body.id,req.body.id_vehiculo])

        console.log(result);

        res.json(result)
        
    } catch (error) {

        console.log(error);
        
    }
}


export const saveGastos= async(req,res)=>{

    try {
        
        const db = await connect()
        console.log(req.user,'noe');
        console.log(req.body);
        let id_usuario=req.user.id
        // if (req.body.motivo != '' || req.body.fecha != '' || req.body.cantidad != '') {
            
            const result = await db.query("INSERT into gastos set ?",[req.body])    
            console.log(result);
    
            res.status(200).json('ticket was saved');   
        // }
        // res.status(400).json('Campos obligatorios')
    

    } catch (error) {

        console.log(error);
        
    }


}
//SELECT * FROM gastos WHERE MONTH(CURRENT_TIMESTAMP())
export const getGastosMonthly= async(req,res)=>{
    try {
        const db= await connect()

        const [result]= await db.query('SELECT * FROM gastos WHERE MONTH(CURRENT_TIMESTAMP()) ORDER BY fecha')

        console.log(result);

        res.json(result)
        
    } catch (error) {

        console.log(error);
        
    }
}


export const deleteGasto= async(req,res)=>{
    try {
        const db = await connect()
        let id= req.params.id
        console.log(id);
        const result = await db.query(`DELETE FROM gastos WHERE id =?`,[id] )
        console.log(result);
        res.status(200).json('gasto was deleted');  
        
    } catch (error) {
        console.log(error);
        
    }
}

export const getGastosbyFechas = async (req,res) =>{
    try {
        const db= await connect()

        // const [result] = await db.query("SELECT c.id_vehiculo, CONCAT_WS(' ',v.marca,v.submarca,v.modelo,v.color,v.placas,v.serie) AS carro,SUM(c.litros) AS litros, SUM(c.costo) AS costos , SUM(c.litros)*SUM(c.costo) AS total FROM combustible c INNER JOIN vehiculo v ON v.id=c.id_vehiculo WHERE  fecha BETWEEN ? AND ? GROUP BY c.id_vehiculo",[req.body.inicial,req.body.final])
        const [result] = await db.query("SELECT * FROM gastos WHERE  fecha BETWEEN ? AND ? order by fecha",[req.body.inicial,req.body.final])

        console.log(result);

        res.json(result)
        
    } catch (error) {
        console.log(error);
        
    }
}


export const getGastosTotal= async(req,res)=>{
    try {
        const db= await connect()

        const [result]= await db.query('SELECT SUM(cantidad) AS suma FROM gastos;')

        console.log(result[0]);

        res.json(result[0])
        
    } catch (error) {

        console.log(error);
        
    }
}