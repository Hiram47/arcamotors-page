import {Router} from 'express'
import { deleteGasto, deleteTicket, getGastosMonthly, getGastosTotal, getGastosbyFechas, getRecordToPay,getReportMonthly, getTicketByUser, getinteresbyDay, getnextmonth, puestoReporte, reporteAllCars, reporteByVehiculo, saveGastos, saveTicket, updateCobro } from '../controllers/ticket.controller.js'
import { authRequired } from '../middlewares/validateToken.js'


const router=Router()

router.post('/ticket',authRequired,saveTicket)
router.get('/ticket/:id',authRequired,getTicketByUser)
router.get('/pay/:id',authRequired,getRecordToPay)
router.delete('/deleteticket/:id',authRequired,deleteTicket)
router.post('/fechavehiculo',authRequired,reporteByVehiculo)
router.get(`/puesto`,authRequired,puestoReporte)
router.post('/fechareporte',authRequired,reporteAllCars)

router.put('/payment/:id',authRequired,updateCobro)

//new
router.post('/month',authRequired,getnextmonth)
router.post('/interesDay',authRequired,getinteresbyDay)
router.post('/reportByCar',authRequired,getReportMonthly)

//new gastos 
router.post('/gastos',authRequired,saveGastos)
router.get('/currentGastos',authRequired,getGastosMonthly)
router.delete('/deleteGasto/:id',authRequired,deleteGasto)
router.post('/gastosFecha',authRequired,getGastosbyFechas)
router.get('/totalGastos',authRequired,getGastosTotal)


export default router