import {Router} from 'express'
import { deleteVehicle, getCarro, getVehicle, getVehicles, saveVehicle, updateVehicle } from '../controllers/tasks.controller.js'
import { authRequired } from '../middlewares/validateToken.js'

import {validateSchema} from '../middlewares/validator.middleware.js'
import {createTaskSchema} from '../schemas/task.schema.js'


const router = Router()

router.get('/tasks',authRequired,getVehicles)
router.get('/tasks/:id',authRequired,getVehicle)
router.get('/taskCarro/:placa',authRequired,getCarro)
router.post('/savevehicle',authRequired,saveVehicle)
// router.post('/savevehicle',validateSchema(createTaskSchema),authRequired,saveVehicle)
router.put('/updatevehicle/:id',authRequired,updateVehicle)
router.delete('/deletevehicle/:id',authRequired,deleteVehicle)

export default router