-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 16-08-2023 a las 20:32:55
-- Versión del servidor: 10.4.22-MariaDB
-- Versión de PHP: 7.4.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `gasstation`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `combustible`
--

CREATE TABLE `combustible` (
  `id` bigint(20) NOT NULL,
  `id_vehiculo` int(11) NOT NULL,
  `fecha` date NOT NULL,
  `litros` varchar(50) COLLATE utf8_bin NOT NULL,
  `costo` varchar(50) COLLATE utf8_bin NOT NULL,
  `km_inicial` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `km_final` varchar(50) COLLATE utf8_bin NOT NULL,
  `no_ticket` varchar(50) COLLATE utf8_bin NOT NULL,
  `comision` varchar(200) COLLATE utf8_bin NOT NULL,
  `id_usuario` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `fecha_registro` varchar(50) COLLATE utf8_bin NOT NULL,
  `usuario_modifico` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `observaciones` varchar(150) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `combustible`
--

INSERT INTO `combustible` (`id`, `id_vehiculo`, `fecha`, `litros`, `costo`, `km_inicial`, `km_final`, `no_ticket`, `comision`, `id_usuario`, `fecha_registro`, `usuario_modifico`, `observaciones`) VALUES
(2, 3, '2023-02-02', '2022', '56', 'naranja', '4555', '84458', 'feh887455', '1', '845587', 'sss', ''),
(3, 3, '2023-07-07', '5345', '654', NULL, '7656', '6654', 'ssp', 'user', '', 'kiko@gmail.com', ''),
(5, 3, '2023-07-13', '44', '4345', NULL, '54545', '5', 'ggggg', 'fg', '12/7/2023', 'kiko@gmail.com', ''),
(6, 3, '2023-07-19', '434', '5465', NULL, '7567', '67', 'dgo', 'dan', '12/7/2023', 'kiko@gmail.com', ''),
(21, 4, '2023-07-06', '43', '54', NULL, '6', '54', '6', 'hy', '12/7/2023', 'kiko@gmail.com', ''),
(24, 3, '2023-07-14', '5345', '75', NULL, '76', '87', 'dgo', 'jgfhjgh', '13/7/2023', 'kiko@gmail.com', ''),
(25, 3, '2023-07-14', '65', '7', NULL, '876', '7', 'j', 'ghj', '13/7/2023', 'kiko@gmail.com', ''),
(26, 3, '0000-00-00', '7567', '876', NULL, '978', '5678', 'hjgjh', 'jghjjjjj', '13/7/2023', 'kiko@gmail.com', ''),
(27, 3, '2023-07-20', '545', '65', NULL, '67567', '456', 'dgo', 'jhghj', '13/7/2023', 'kiko@gmail.com', ''),
(28, 18, '2023-07-14', '54', '6546', NULL, '8456756', '68456', 'dgo', 'jfghj', '13/7/2023', 'kiko@gmail.com', ''),
(29, 3, '2023-08-01', '45', '654', NULL, '65', '7765', 'dgo', 'hyfgy', '1/8/2023', 'kiko@gmail.com', ''),
(30, 10, '2023-08-01', '50', '25.5', NULL, '35345', '534', 'dgo', 'rugal', '1/8/2023', 'kiko@gmail.com', ''),
(31, 3, '2023-08-02', '65', '26.5', NULL, '558452', '1524885', 'lerdo', 'jon', '2/8/2023', 'kiko@gmail.com', ''),
(32, 3, '2023-08-02', '62', '25.23', NULL, '48574', '4234', 'mapimi', 'jon', '2/8/2023', 'kiko@gmail.com', ''),
(33, 5, '2023-08-02', '65', '26.3', NULL, '53456', '8346545', 'dgo', 'arthur', '2/8/2023', 'kiko@gmail.com', ''),
(34, 21, '2023-08-02', '40', '20', NULL, '21343234', '12343', 'Durango', 'Carlos', '2/8/2023', 'ssp@gmail.com', ''),
(35, 3, '2023-08-07', '55', '26.5', NULL, '65575', '6665', 'dgo', 'france', '7/8/2023', 'kiko@gmail.com', ''),
(36, 3, '2023-08-07', '54', '25.5', NULL, '6456456', '78567', 'gomez', 'jon', '7/8/2023', 'kiko@gmail.com', 'extra'),
(37, 3, '2023-08-07', '67', '25.23', NULL, '645734', '7654', 'dgo', 'jon constantine', '7/8/2023', 'kiko@gmail.com', 'carga extra');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `puesto`
--

CREATE TABLE `puesto` (
  `id` int(11) NOT NULL,
  `cargo` varchar(100) COLLATE utf8_bin NOT NULL,
  `nombre` varchar(255) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `puesto`
--

INSERT INTO `puesto` (`id`, `cargo`, `nombre`) VALUES
(1, 'Administrador de la policia', 'C.P Victor Gomez Burgos');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `id` int(11) NOT NULL,
  `username` varchar(100) COLLATE utf8_bin NOT NULL,
  `email` varchar(100) COLLATE utf8_bin NOT NULL,
  `password` varchar(150) COLLATE utf8_bin NOT NULL,
  `rol` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`id`, `username`, `email`, `password`, `rol`) VALUES
(1, 'world', 'zoro@gmail.com', '123', 1),
(2, 'world2', 'zoro2@gmail.com', '1232', 1),
(3, 'world23', 'zoro2@gmail.com3', '12323', 1),
(4, 'world23w', 'zoro2@gmail.com3w', '12323w', 1),
(5, 'world23wff', 'zoro2@gmail.com3wff', '12323wff', 1),
(6, 'jon', 'zoro2@gmail.com3wff', '$2a$10$YXYmF47pqvAQAdXSJ9T7WeoTt82q3sZasl02OO4jFxAywlfLFxX..', 1),
(11, 'jon2', 'zoro2@gmail.com3wff2', '$2a$10$6Wf111b0d/abIl/Z4G20xe0EX21eAaxzzskbmG6auLc0mBlBiRdCK', 1),
(12, 'jon24', 'zoro2@gmail.com3wff24', '$2a$10$UnsQbHzDOZCVxXEbxhdCQeh0qhzNcROXhq6zSJVBc444IL.34mKHG', 1),
(13, 'jon24', 'zoro2@gmail.com3wff24', '$2a$10$B2jqXSX2OZvunBHrPwLthevpMDf45AvVuSBxBDBn68ucuvWJJVJJ2', 1),
(14, 'jon24', 'zoro2@gmail.com3wff24', '$2a$10$MyYgBN4WKCHNOx85wVtIg.MXqisZJRQNB6yelSukk8SsSiOQQ.JXi', 1),
(15, 'jon24', 'zoro2@gmail.com3wff24', '$2a$10$NYcvnxB7cir/yPQUsi3pgejzAs/wbh3K9j0YwoaAv8o08J9O5CXEO', 1),
(16, 'jon24', 'zoro2@gmail.com3wff24', '$2a$10$zFs9D2arGzDbHHn/Lk.rVe98X0VOU/yufesBODQR3uA9vMS7unTW6', 1),
(17, 'juan', 'juan@gmail.com', '$2a$10$Pd6dJ9ESKFNyPF/HkBC2JeWP.uHwa2B7YyxhximFk7pxVonAX5JLq', 1),
(18, 'hil', 'hil@gmail.com', '$2a$10$oEVhJ3MQn6aIdHcQnGffmu.NU3N7PaFKbdgtuOkqK5K7BIVeD7JlG', 1),
(19, 'will', 'will@gmail.com', '$2a$10$EIBZI.fQq3Qv7NqXax7xQe9fJ1eFrLTSh3W.2Gz411ZbQji8B6C5G', NULL),
(20, 'kiko', 'kiko@gmail.com', '$2a$10$CzyNu4D4OPfZx5ginQrpAeb97O0Y6vjdIozE/tWwvYIZ.xtgnuy0.', NULL),
(21, 'witcher', 'witcher@gmail.com', '$2a$10$lR7UsaeYf0rEChiluMkFhOxw/OjmNioxHDUql5KTv.lBeXlGeiqku', NULL),
(22, 'wade', 'wade@durango.gob.mx', '$2a$10$dNvdWlRCLcs0qUl3qvBoleewdrjzIoTbgQeElqIwYsO9.5pMguDo2', NULL),
(23, 'df', 'luffy4@gmail.com', '$2a$10$D1/kRj0/SyfsZFbAI5NKU.CoZP8O8G25NnEXW3ID/zB5Y8AwuQbk.', NULL),
(26, 'dd', 'kiko@gmail.com', '$2a$10$UG7uZgHfiSCLgB1i12ubrO72cZJvwluDGezEfbvDwyVS3PKp8yLqm', NULL),
(29, 'eazy', 'eazy@gmail.com', '$2a$10$KxIg6cVY2luz9fEUCVRHR.aOF1cAYBvQ2XGnuZQ6/1U02kinEIfGi', NULL),
(30, 'jon', 'jon@gmail.com', '$2a$10$xmbR4GUsG4SQiiSH0f4s/OUUwuybiwwDChwedgWrabZuIlZHjBGo.', NULL),
(31, 'ssp', 'ssp@gmail.com', '$2a$10$cq779EzMCEQ3DIJwL7I1Nu7CNSh08RQnbyHzrK0oJER9nqYGmQ8q.', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `vehiculo`
--

CREATE TABLE `vehiculo` (
  `id` int(11) NOT NULL,
  `marca` varchar(50) COLLATE utf8_bin NOT NULL,
  `submarca` varchar(50) COLLATE utf8_bin NOT NULL,
  `modelo` varchar(50) COLLATE utf8_bin NOT NULL,
  `tipo` varchar(50) COLLATE utf8_bin NOT NULL,
  `color` varchar(50) COLLATE utf8_bin NOT NULL,
  `no_motor` varchar(50) COLLATE utf8_bin NOT NULL,
  `serie` varchar(50) COLLATE utf8_bin NOT NULL,
  `placas` varchar(50) COLLATE utf8_bin NOT NULL,
  `inventario` varchar(50) COLLATE utf8_bin NOT NULL,
  `id_usuario` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `usuario_modifico` varchar(100) COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `vehiculo`
--

INSERT INTO `vehiculo` (`id`, `marca`, `submarca`, `modelo`, `tipo`, `color`, `no_motor`, `serie`, `placas`, `inventario`, `id_usuario`, `usuario_modifico`) VALUES
(1, 'FERRARI F50', 'Enzo', '2008', 'DEPORTIVO', 'RED', '4555', '84458', 'FRJUSD', '84558', '', 'kiko@gmail.com'),
(3, 'lambo', 'murcielago', '2012', 'deportivo', 'amarillo', '4555', '84458', 'feh887455', '845587', NULL, NULL),
(4, 'lambo', 'reventon', '2012', 'deportivo', 'blanco', '4555', '84458', 'feh887455', '845587', '1', NULL),
(5, 'lambo', 'veneno', '2012', 'deportivo', 'negro', '4555', '84458', 'feh887455', '845587', '1', NULL),
(6, 'lambo v12', 'veneno', '2022', 'deportivo', 'naranja', '4555', '84458', 'feh887455', '845587', '1', NULL),
(7, 'lambo v12', 'veneno', '2022', 'deportivo', 'naranja', '4555', '84458', 'feh887455', '845587', '1', NULL),
(8, 'lambo v12', 'veneno', '2022', 'deportivo', 'naranja', '4555', '84458', 'feh887455', '845587', NULL, NULL),
(9, 'Prueba', 'test', 'dddd', 'dfg', 'dfg', 'dfg777', 'gfhdfg', 'hert', 'hert', '', 'ssp@gmail.com'),
(10, 'aston martin', 'db9', '2008', 'deportivo', 'plata', '652', '5gfdfgdf', 'dg456', 'gdfg', '0', NULL),
(11, 'hdfg', 'dsfgsdf', 'hdsfgh', 'dfggf', 'hhhfg', '5rfret', 'hgstjsfg5', 'hssdfg', 'fdfgdfg', '0', NULL),
(12, 'fgggg', 'dhfgh', 'dghdfg', 'hdsf', 'fghdfgh', 'sdfgsdf', 'hsfdgdf', 'ghsdfg', 'sdfgdsfg', '0', NULL),
(13, 'gdfg', 'hfdgh', 'ddfg', 'hdfgh', 'dfgh', 'dgfhdf', 'gfhdfg', 'hfg', 'dfghdf', '0', NULL),
(14, 'ford', 'mustang', 'jhgf', 'fghj', 'fghjfghjk', 'fghdfg', 'adsgdf4534', 'ghdfgh', 'dfgjhdkjtry4', '', 'kiko@gmail.com'),
(15, 'bugatti', 'gsdfg', 'hsfdgsdf', 'ghsdfg', 'fgggs', 'sfg', 'sfdgsdfg', 'ashadfg', 'sdfgsdfg', '0', NULL),
(16, 'mazda3', 'gfsdgsdf', 'gfsdfg', 'sfdgsdf', 'hhhgsdfg', 'sdfg54', 'gsdfg453434', 'ggg', 'g434', '0', NULL),
(17, 'FORD', 'rio', 'hsdfg', 'hsdfg', 'sdfg', 'hfsdfg', 'sdfgsdfg', 'sdfgsdfg', 'hsdfg5435', '', 'hil@gmail.com'),
(18, 'chevy', 'gsdfg', 'hdfgh', 'jdfgh', 'jdfghd', 'jdfgh', 'jdfgh', 'jdfgh', 'jdfgh', '0', NULL),
(19, 'dodge', 'durango', '2020', 'van', 'negro', '5345', 'tgwer65434', 'dg5345', '45345', '', 'kiko@gmail.com'),
(20, 'chevrolet', 'corsa', '2003', 'SEDAN', 'rojo', 'gf345', 'fygd45345', 'dg67456', '435345', '', 'kiko@gmail.com'),
(21, 'Ford', 'F-150', '1999', 'Pick up', 'Negra', '2134324', 'a21334dfqwer434', 'asdg234d', 'xcris', '', 'ssp@gmail.com'),
(22, 'DODGE', 'RAM 1500', '2022', 'PICK UP', 'AZUL/GRIS', '370', '3C6SRBDTXNG414282', 'DG-160P-2', '10-4788', 'ssp@gmail.com', '');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `combustible`
--
ALTER TABLE `combustible`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `puesto`
--
ALTER TABLE `puesto`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `vehiculo`
--
ALTER TABLE `vehiculo`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `combustible`
--
ALTER TABLE `combustible`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;

--
-- AUTO_INCREMENT de la tabla `puesto`
--
ALTER TABLE `puesto`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT de la tabla `vehiculo`
--
ALTER TABLE `vehiculo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
