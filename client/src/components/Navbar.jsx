import React from 'react'
import { Link } from 'react-router-dom'
import { useAuth } from '../context/AuthContext'
import arca from '../assets/arca.png'

export const Navbar = () => {

    const {isAuthenticaded,user,logout}=useAuth()

  return (
    <nav className='bg-zinc-700 my-3 flex justify-between py-5 px-10 rounded-lg'>
        <Link to={isAuthenticaded ? '/vehiculo' : '/'}>
            <h1 className='text-3xl font-bold flex justify-between ' ><img style={{width:'80px',height:'35px'}} className='px-1' src={arca} alt="arcamotors" /> </h1>
        </Link>
        <ul className='flex gap-x-2'>
            {
                isAuthenticaded? (
                <>
                <li>Bienvenido {user.username.toUpperCase()}</li>
                <li><Link className='bg-red-800 hover:bg-red-600 px-4 py-1 rounded-sm' to='/vehiculo'>Vehiculos</Link></li>
                <li><Link className='bg-red-800 hover:bg-red-600 px-4 py-1 rounded-sm' to='/add-vehiculo'>Agregar vehiculo</Link></li>
                {/* <li><Link className='bg-red-800 hover:bg-red-600 px-4 py-1 rounded-sm' to='/reports'>Reportes</Link></li> */}
                <li><Link className='bg-red-800 hover:bg-red-600 px-4 py-1 rounded-sm' to='/cotizar'>Cotizar</Link></li>
                <li><Link className='bg-red-800 hover:bg-red-600 px-4 py-1 rounded-sm' to='/gastos'>Gastos</Link></li>
                <li><Link className='bg-red-950 hover:bg-red-600 px-4 py-1 rounded-sm' to='/' onClick={() =>{logout()}}>Logout</Link></li>

                </>):
                (
                    <>
                    <li ><Link className='bg-red-800 hover:bg-red-600 px-4 py-1 rounded-sm' to='/login'>Login </Link></li>
                    {/* <li ><Link className='bg-red-800 hover:bg-red-600 px-4 py-1 rounded-sm' to='/register'>Register </Link></li> */}
                    </>

                )
            }
        </ul>
        

    </nav>
  )
}
