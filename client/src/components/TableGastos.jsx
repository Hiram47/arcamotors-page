import React from 'react'
import { Link } from 'react-router-dom';

import dayjs from 'dayjs'
import utc from 'dayjs/plugin/utc'
import { useTicket } from '../context/TicketContext';
import { toast } from 'sonner';
// import { deleteGasto } from '../../../src/controllers/ticket.controller';

dayjs.locale('es')
dayjs.extend(utc)

export const TableGastos = ({gasto,i,totald}) => {

  const {deleteGastos}= useTicket()
  
  let anidado=0
  console.log(totald,'TOTAL');
    const ondelete = async( id)=>{
      //  await deleteTicketGas(id)
      toast('Estas seguro de eliminar el elemento?',{
        action:{
            label:'Aceptar',
            onClick: async()=>{
                const res=await deleteGastos(id)
                  console.log(id);
                  console.log(res);
                console.log(id);
            }
        },
      })
       // setload(false)
    }

    console.log(gasto.cantidad);
    anidado+=Number(gasto.cantidad)
    console.log(anidado, 'anidado');
    const nuevo= gasto.cantidad
   // console.log(nuevo.reducer((a,b)=> a+b,0))
  // console.log(gasto.sum('cantidad'));
  return (
    <tr className='bg-white border-b dark:bg-zinc-800 dark:border-gray-800 hover:bg-gray-50 dark:hover:bg-gray-600'
    // className='border-b dark:border-neutral-500' 
    >

    <td className='whitespace-nowrap  px-4 py-2 font-sans'>{i+1}</td>
      <td className='px-2 py-2'>{gasto.motivo.toUpperCase()}</td>
      <td className='px-2 py-2'>{dayjs.utc(gasto.fecha ).locale('es').format('DD/MM/YYYY')}</td>
      <td className='px-2 py-2'>$ {new Intl.NumberFormat().format(gasto.cantidad)}</td>
      <td className='px-2 py-2'>$ {new Intl.NumberFormat().format(totald)}</td>
      <td  className='px-2 py-2 rounded-md  text-white  font-bold'><i style={{fontSize:'25px'}} className="fa fa-trash "  onClick={()=>ondelete(gasto.id)} ></i></td>
      {/* <td><button className='bg-red-800 hover:bg-red-600 p-1 m-2 rounded-md font-bold' onClick={()=>'hola'}>Eliminar</button></td> */}
      {/* <td  className='px-2 py-2'><Link className='bg-red-500 hover:bg-red-600 rounded-md min-w-full text-white  font-bold p-2' to={`/vehiculo/${gasto.id}`}><i style={{fontSize:'25px'}} className="fa fa-trash" ></i></Link></td> */}

     
</tr>  
  )
}