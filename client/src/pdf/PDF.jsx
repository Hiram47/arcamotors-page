// import React from 'react'
import {Page,Text,Document,StyleSheet, View,Image} from '@react-pdf/renderer'


import dayjs from 'dayjs'
import utc from 'dayjs/plugin/utc'
import {es} from 'dayjs/locale/es-mx'
import { useAuth } from '../context/AuthContext'
import ssp2 from '../assets/ssp.png'
// import ssp from '../assets/ssp2.jpg'
dayjs.locale('es')
dayjs.extend(utc)

export const PDF = ({car,vehiculo,fechas,puestoPolicia,user}) => {
    const styles = StyleSheet.create({
        table: { 
          display: "table", 
          width: "auto", 
          borderStyle: "solid", 
          borderWidth: 1, 
          borderRightWidth: 0, 
          borderBottomWidth: 0 
        }, 
        tableRow: { 
          margin: "auto", 
          flexDirection: "row" 
        }, 
        tableCol: { 
          width: "14.3%", 
          borderStyle: "solid", 
          borderWidth: 1, 
          borderLeftWidth: 0, 
          borderTopWidth: 0 
        }, 
        tableCell: { 
          margin: "auto", 
          marginTop: 5, 
          fontSize: 12 
        },
        tableCell2: { 
            margin: "auto", 
            marginTop: 5, 
            fontSize: 12 ,
            
          }
      });
  return (

    <Document >
    <Page fixed size={'LETTER'} style={{padding:20}}>
      <Image src={ssp2} style={{height:'50px',width:'45px'}}/>
      {/* <Image src={ssp} style={{height:'60px',width:'120px',position:'absolute',left:440 ,top:'10px'}}/> */}
        <Text style={{fontSize:22,paddingVertical:15,fontWeight:'heavy'}} >Reporte vehiculo</Text>
        <View style={{display:'flex',alignItems:'stretch'}}>

            <Text style={{fontSize:12,paddingVertical:2,fontWeight:'heavy'}}  >Marca: {vehiculo.marca}</Text>
            <Text style={{fontSize:12,paddingVertical:2,fontWeight:'heavy', flexDirection: 'row'}}  >Submarca: {vehiculo.submarca}</Text>
            <Text style={{fontSize:12,paddingVertical:2,fontWeight:'heavy'}}  >Modelo: {vehiculo.modelo}</Text>
            <Text style={{fontSize:12,paddingVertical:2,fontWeight:'heavy'}}  >Tipo: {vehiculo.tipo}</Text>
            <Text style={{fontSize:12,paddingVertical:2,fontWeight:'heavy'}}  >Color: {vehiculo.color}</Text>
            <Text style={{fontSize:12,paddingVertical:2,fontWeight:'heavy'}}  >Placa: {vehiculo.plcas}</Text>
        </View>
        <View style={{display:'flex',alignItems:'flex-end',position:'absolute',top:73,right:50 }}>

            <Text style={{fontSize:12,paddingVertical:2,fontWeight:'heavy'}}  >Serie: {vehiculo.serie}</Text>
            <Text style={{fontSize:12,paddingVertical:2,fontWeight:'heavy'}}  >Inventario: {vehiculo.inventario}</Text>
            <Text style={{fontSize:12,paddingVertical:2,fontWeight:'heavy'}}  >Economico: {vehiculo.no_motor}</Text>

            <Text style={{fontSize:12,paddingVertical:2,fontWeight:'heavy'}}  >Fecha inicial: {fechas.inicial}</Text>
            <Text style={{fontSize:12,paddingVertical:2,fontWeight:'heavy'}}  >Fecha final: {fechas.final}</Text>
        </View>



      <View style={styles.table}> 
        <View style={styles.tableRow}> 
          <View style={styles.tableCol}> 
            <Text style={styles.tableCell}>Fecha</Text> 
          </View> 
          <View style={styles.tableCol}> 
            <Text style={styles.tableCell}>Litros</Text> 
          </View> 
          <View style={styles.tableCol}> 
            <Text style={styles.tableCell}>Costo</Text> 
          </View> 
          <View style={styles.tableCol}> 
            <Text style={styles.tableCell}>Kilometraje</Text> 
          </View> 
          <View style={styles.tableCol}> 
            <Text style={styles.tableCell}>No. ticket</Text> 
          </View> 
          <View style={styles.tableCol}> 
            <Text style={styles.tableCell}>Comision</Text> 
          </View> 
          
          <View style={styles.tableCol}> 
            <Text style={styles.tableCell}>Usuario</Text> 
          </View>
        </View>
   
          {
              
              car.map(c=>(
        <View style={styles.tableRow}> 
         <View style={styles.tableCol}>
            <Text style={styles.tableCell}>{dayjs.utc(c.fecha ).locale('es').format('DD/MM/YYYY')}</Text> 
          </View>
          <View style={styles.tableCol}>
             <Text style={styles.tableCell}>{c.litros}</Text> 
         </View>
         <View style={styles.tableCol}>
             <Text style={styles.tableCell}>{c.costo}</Text> 
         </View>
         <View style={styles.tableCol}>
             <Text style={styles.tableCell}>{c.km_final}</Text> 
         </View>
         <View style={styles.tableCol}>
             <Text style={styles.tableCell}>{c.no_ticket}</Text> 
         </View>
         <View style={styles.tableCol}>
             <Text style={styles.tableCell}>{c.comision}</Text> 
         </View>
         <View style={styles.tableCol}>
             <Text style={styles.tableCell}>{c.id_usuario}</Text> 
         </View>
      
        </View>  
    ))
          }
      </View>
      {/* <Text style={StyleSheet.create({fontSize: 24,fontWeight:'bold'})}>HOLA</Text> */}
          {/* texto centrado esta comentado */}
      {/* <Text style={{fontSize:12, left:230,top:700,position:'absolute',fontWeight:'bold'}} >{puestoPolicia.nombre}</Text>
      <Text style={{fontSize:12, left:230,top:715,position:'absolute'}} >{puestoPolicia.cargo}</Text> */}

      <Text style={{fontSize:12, left:100,top:700,position:'absolute',fontWeight:'bold'}} >{puestoPolicia.nombre}</Text>
      <Text style={{fontSize:12, left:100,top:715,position:'absolute'}} >{puestoPolicia.cargo}</Text>

      <Text style={{fontSize:12, left:380,top:700,position:'absolute',fontWeight:'bold'}} >{user.username}</Text>
      <Text style={{fontSize:12, left:380,top:715,position:'absolute'}} >Realizo reporte</Text>
    </Page>
  </Document>


   

   
  )
}
