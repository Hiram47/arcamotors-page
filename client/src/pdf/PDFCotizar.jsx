// import React from 'react'
import {Page,Text,Document,StyleSheet, View,Image} from '@react-pdf/renderer'


import dayjs from 'dayjs'
import utc from 'dayjs/plugin/utc'
import {es} from 'dayjs/locale/es-mx'
import { useAuth } from '../context/AuthContext'
import ssp2 from '../assets/styleBW2.png'
// import ssp from '../assets/ssp2.jpg'
dayjs.locale('es')
dayjs.extend(utc)

export const PDFCotizar = ({datavehiculo='hola',user}) => {
    const styles = StyleSheet.create({
        table: { 
          display: "table", 
          width: "auto", 
          borderStyle: "solid", 
          borderWidth: 1, 
          borderRightWidth: 0, 
          borderBottomWidth: 0 ,
          
        }, 
        tableRow: { 
          margin: "auto", 
          flexDirection: "row" ,
          // backgroundColor:'gray'
        }, 
        tableCol: { 
          width: "14.3%", 
          borderStyle: "solid", 
          borderWidth: 1, 
          borderLeftWidth: 0, 
          borderTopWidth: 0 
        }, 
        tableCell: { 
          margin: "auto", 
          marginTop: 5, 
          fontSize: 12 ,

        },
        tableCell2: { 
            margin: "auto", 
            marginTop: 5, 
            fontSize: 12 ,
            
          }
      });
  return (

    <Document >
    <Page fixed size={'LETTER'} style={{padding:20}}>
      <Image src={ssp2} style={{height:'50px',width:'95px'}}/>
      {/* <Image src={ssp} style={{height:'60px',width:'120px',position:'absolute',left:440 ,top:'10px'}}/> */}
        

        <Text style={{fontSize:22,paddingVertical:15,left:220,fontWeight:'heavy'}} >Cotización </Text>
        <View style={{display:'flex',alignItems:'stretch'}}>

            {/* <Text style={{fontSize:14,paddingVertical:3,fontWeight:'heavy'}}  >Vehículo: {datavehiculo.vehiculo?.toUpperCase()} </Text>
            <Text style={{fontSize:14,paddingVertical:3,fontWeight:'heavy', flexDirection: 'row'}}  >Precio: ${new Intl.NumberFormat().format(datavehiculo.venta)} </Text>
            <Text style={{fontSize:14,paddingVertical:3,fontWeight:'heavy'}}  >Enganche: $ {new Intl.NumberFormat().format(datavehiculo.enganche)} </Text>
            <Text style={{fontSize:14,paddingVertical:3,fontWeight:'heavy'}}  >Interes %: {datavehiculo.interes}</Text>
            <Text style={{fontSize:14,paddingVertical:3,fontWeight:'heavy'}}  >Meses: {datavehiculo.meses}</Text>
            <Text style={{fontSize:14,paddingVertical:3,fontWeight:'heavy'}}  >Mensualidad: ${new Intl.NumberFormat().format(datavehiculo.operacion2.toFixed(2))} </Text> */}
            {/* new Intl.NumberFormat().format(task.costo) */}
        </View>
        <View style={{display:'flex',alignItems:'flex-end',position:'absolute',top:73,right:50 }}>

            {/* <Text style={{fontSize:12,paddingVertical:2,fontWeight:'heavy'}}  >Serie: </Text>
            <Text style={{fontSize:12,paddingVertical:2,fontWeight:'heavy'}}  >Inventario: </Text>
            <Text style={{fontSize:12,paddingVertical:2,fontWeight:'heavy'}}  >Economico: </Text> */}

            <Text style={{fontSize:12,paddingVertical:2,fontWeight:'heavy'}}  >Fecha : {datavehiculo.fulldate}</Text>
            {/* <Text style={{fontSize:12,paddingVertical:2,fontWeight:'heavy'}}  >Fecha final: </Text> */}
        </View>



       <View style={styles.table}> 
        <View style={styles.tableRow}> 
          <View style={styles.tableCol}> 
            <Text style={styles.tableCell}>Vehículo</Text> 
          </View> 
          <View style={styles.tableCol}> 
            <Text style={styles.tableCell}>Precio</Text> 
          </View> 
          <View style={styles.tableCol}> 
            <Text style={styles.tableCell}>Enganche</Text> 
          </View> 
          <View style={styles.tableCol}> 
            <Text style={styles.tableCell}>Interes %</Text> 
          </View> 
          <View style={styles.tableCol}> 
            <Text style={styles.tableCell}>Meses</Text> 
          </View> 
          <View style={styles.tableCol}> 
            <Text style={styles.tableCell}>Mensualidad</Text> 
          </View> 
          
          <View style={styles.tableCol}> 
            <Text style={styles.tableCell}>Atendio</Text> 
          </View>
        </View>



        <View style={styles.tableRow}> 
         <View style={styles.tableCol}>
            <Text style={styles.tableCell}>{datavehiculo.vehiculo?.toUpperCase()}</Text> 
          </View>
          <View style={styles.tableCol}>
             <Text style={styles.tableCell}> ${new Intl.NumberFormat().format(datavehiculo.venta)}</Text> 
         </View>
         <View style={styles.tableCol}>
             <Text style={styles.tableCell}> $ {new Intl.NumberFormat().format(datavehiculo.enganche)}</Text> 
         </View>
         <View style={styles.tableCol}>
             <Text style={styles.tableCell}>{datavehiculo.interes/10}</Text> 
         </View>
         <View style={styles.tableCol}>
             <Text style={styles.tableCell}>{datavehiculo.meses}</Text> 
         </View>
         <View style={styles.tableCol}>
             <Text style={styles.tableCell}> ${new Intl.NumberFormat().format(datavehiculo.operacion2.toFixed(2))}</Text> 
         </View>
         <View style={styles.tableCol}>
             <Text style={styles.tableCell}>{user.username?.toUpperCase()}</Text> 
         </View>
      
        </View>  
   
     
      </View> 
      {/* <Text style={StyleSheet.create({fontSize: 24,fontWeight:'bold'})}>HOLA</Text> */}
          {/* texto centrado esta comentado */}
      {/* <Text style={{fontSize:12, left:230,top:700,position:'absolute',fontWeight:'bold'}} >{puestoPolicia.nombre}</Text>
      <Text style={{fontSize:12, left:230,top:715,position:'absolute'}} >{puestoPolicia.cargo}</Text> */}

      <Text style={{fontSize:12, left:100,top:700,position:'absolute',fontWeight:'bold'}} ></Text>
      <Text style={{fontSize:12, left:100,top:715,position:'absolute'}} ></Text>

      <Text style={{fontSize:12, left:380,top:700,position:'absolute',fontWeight:'bold'}} ></Text>
      <Text style={{fontSize:12, left:380,top:715,position:'absolute'}} >Atendio: {user.username}</Text>

      <Text style={{fontSize:12, left:80,top:715,position:'absolute'}} >Tel. 618-234-65-44 </Text>
      <Text style={{fontSize:12, left:80,top:730,position:'absolute'}} >Ubicación: Frente al colegio REX</Text>


 
        
    </Page>
  </Document>


   

   
  )
}
