import {Page,Text,Document,StyleSheet, View,Image} from '@react-pdf/renderer'
import ssp2 from '../assets/styleBW2.png'

export const PDFHistorial = ({tickets}) => {

    const styles = StyleSheet.create({
        table: { 
          display: "table", 
          width: "auto", 
          borderStyle: "solid", 
          borderWidth: 1, 
          borderRightWidth: 0, 
          borderBottomWidth: 0 
        }, 
        tableRow: { 
          margin: "auto", 
          flexDirection: "row" 
        }, 
        tableCol: { 
          width: "11.1%", 
          borderStyle: "solid", 
          borderWidth: 1, 
          borderLeftWidth: 0, 
          borderTopWidth: 0 
        }, 
        tableCell: { 
          margin: "auto", 
          marginTop: 5, 
          fontSize: 13 ,

        },
        tableCell2: { 
            margin: "auto", 
            marginTop: 5, 
            fontSize: 10 ,
            
          }
      });

  return (

    <Document>
        <Page fixed size={'LETTER' } orientation="landscape" style={{padding:20}}>
            <Image src={ssp2} style={{height:'50px',width:'95px'}}/>
            <Text style={{fontSize:22,paddingVertical:15,left:220,fontWeight:'heavy'}} >Historial de Pagos </Text>

            <View style={styles.table}>
                <View style={styles.tableRow}>
                    <View style={styles.tableCol}>
                        {/* <Text style={styles.tableCell}>Text</Text> */}

                        <Text style={styles.tableCell}>Precio</Text>
                    </View>
                    <View style={styles.tableCol}>

                        <Text style= {styles.tableCell} >Enganche</Text>
                    </View>
                    <View style={styles.tableCol}>

                        <Text style={styles.tableCell} >Meses</Text>
                    </View>
                    <View style={styles.tableCol}>

                      <Text style={styles.tableCell} >Mensualidad</Text>
                    </View>

                     <View style={styles.tableCol}>

                        <Text style={styles.tableCell}>No. Pagos</Text>
                    </View>
                    <View style={styles.tableCol}>

                        <Text style={styles.tableCell} >No. Pagos faltantes</Text>
                    </View>
                    <View style={styles.tableCol}>

                        <Text style={styles.tableCell} >Calculo sin interes</Text>
                    </View>
                    <View style={styles.tableCol}>

                      <Text style={styles.tableCell}>Precio s/n interes</Text>
                    </View>
                    <View style={styles.tableCol}>

                    <Text style={styles.tableCell}>Precio c/n interes</Text>
                    </View>
                </View>

                {
                    tickets.map(ticket=>(
                        <View style={styles.tableRow}>

                            <View style={styles.tableCol}>
                                 <Text style={styles.tableCell}>${new Intl.NumberFormat().format(ticket.venta2)}</Text> 
                             </View>
                             <View style={styles.tableCol}>
                                 <Text style={styles.tableCell}>${new Intl.NumberFormat().format(ticket.enganche)}</Text> 
                             </View>
                             <View style={styles.tableCol}>
                                 <Text style={styles.tableCell}>{ticket.meses}</Text> 
                             </View>
                             <View style={styles.tableCol}>
                                 <Text style={styles.tableCell}>${new Intl.NumberFormat().format(ticket.mensualidad.toFixed(2))}</Text> 
                             </View>
                             <View style={styles.tableCol}>
                                 <Text style={styles.tableCell}>{ticket.pagos}</Text> 
                             </View>
                             <View style={styles.tableCol}>
                                 <Text style={styles.tableCell}>{new Intl.NumberFormat().format(ticket.meses-ticket.pagos)}</Text> 
                             </View>
                             <View style={styles.tableCol}>
                                 <Text style={styles.tableCell}>{new Intl.NumberFormat().format((ticket.venta2-ticket.enganche)/ticket.meses)}</Text> 
                             </View>
                             <View style={styles.tableCol}>
                                 <Text style={styles.tableCell}>${new Intl.NumberFormat().format((ticket.meses.toFixed(2)-ticket.pagos.toFixed(2))*(ticket.venta2.toFixed(2)-ticket.enganche.toFixed(2))/ticket.meses.toFixed(2))}</Text> 
                             </View>

                             <View style={styles.tableCol}>
                                 <Text style={styles.tableCell}>${new Intl.NumberFormat().format(ticket.mensualidad*ticket.meses)}</Text> 
                             </View>

                            
                        </View>
                    ))
                }



            </View>





            <Text style={{fontSize:12, left:45,top:540,position:'absolute'}} >Tel. 618-234-65-44 </Text>
            <Text style={{fontSize:12, left:45,top:520,position:'absolute'}}  >Ubicación: Frente al colegio REX</Text>


        </Page>

    </Document>

  )

}
