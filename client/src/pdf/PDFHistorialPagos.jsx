import React from 'react'
import {Image, Page, Text, View, Document, StyleSheet } from '@react-pdf/renderer';
import ssp2 from '../assets/styleBW2.png'

export const PDFHistorialPagos = ({history}) => {
    const styles = StyleSheet.create({
        table: { 
          display: "table", 
          width: "auto", 
          borderStyle: "solid", 
          borderWidth: 1, 
          borderRightWidth: 0, 
          borderBottomWidth: 0 
        }, 
        tableRow: { 
          margin: "auto", 
          flexDirection: "row" 
        }, 
        tableCol: { 
          width: "12.5%", 
          borderStyle: "solid", 
          borderWidth: 1, 
          borderLeftWidth: 0, 
          borderTopWidth: 0 
        }, 
        tableCell: { 
          margin: "auto", 
          marginTop: 5, 
          fontSize: 13 ,

        },
        tableCell2: { 
            margin: "auto", 
            marginTop: 5, 
            fontSize: 10 ,
            
          }
      });



  return (

    <Document>
    <Page fixed size={'LETTER' } orientation="landscape" style={{padding:20}}>
        <Image src={ssp2} style={{height:'50px',width:'95px'}}/>
        <Text style={{fontSize:22,paddingVertical:15,left:220,fontWeight:'heavy'}} >Historial de Pagos </Text>

        <View style={styles.table}>
            <View style={styles.tableRow}>
                <View style={styles.tableCol}>
                    {/* <Text style={styles.tableCell}>Text</Text> */}

                    <Text style={styles.tableCell}>Precio</Text>
                </View>
                <View style={styles.tableCol}>

                    <Text style= {styles.tableCell} >Pagando</Text>
                </View>
                <View style={styles.tableCol}>

                    <Text style={styles.tableCell} >Faltante</Text>
                </View>
                <View style={styles.tableCol}>

                  <Text style={styles.tableCell} >Meses</Text>
                </View>

                 <View style={styles.tableCol}>

                    <Text style={styles.tableCell}>Interes %</Text>
                </View>
                <View style={styles.tableCol}>

                    <Text style={styles.tableCell} >Mensualidad</Text>
                </View>
                <View style={styles.tableCol}>

                    <Text style={styles.tableCell} >Pagos</Text>
                </View>
                <View style={styles.tableCol}>

                  <Text style={styles.tableCell}>Fecha</Text>
                </View>
                {/* <View style={styles.tableCol}>

                <Text style={styles.tableCell}>Precio c/n interes</Text>
                </View> */}
            </View>
            {
                // console.log(history)
                history.map(rep =>(
                    <View style={styles.tableRow}>

                        <View style={styles.tableCol}>
                            <Text style={styles.tableCell}>${new Intl.NumberFormat().format(rep.mensualidad * rep.meses)}</Text> 
                        </View>
                        <View style={styles.tableCol}>
                            <Text style={styles.tableCell}>${new Intl.NumberFormat().format(rep.pago * rep.mensualidad)}</Text> 
                        </View>
                        <View style={styles.tableCol}>
                            <Text style={styles.tableCell}>${new Intl.NumberFormat().format((rep.mensualidad*rep.meses)-(rep.pago*rep.mensualidad))}</Text> 
                        </View>

                        <View style={styles.tableCol}>
                            <Text style={styles.tableCell}>{rep.meses}</Text> 
                        </View>
                        <View style={styles.tableCol}>
                            <Text style={styles.tableCell}>{rep.interes/10}</Text> 
                        </View>
                        <View style={styles.tableCol}>
                            <Text style={styles.tableCell}>${new Intl.NumberFormat().format((rep.mensualidad))}</Text> 
                        </View>
                        <View style={styles.tableCol}>
                            <Text style={styles.tableCell}>{rep.pago}</Text> 
                        </View>
                        <View style={styles.tableCol}>
                            <Text style={styles.tableCell}>{rep.fecha_registro}</Text> 
                        </View>
                    </View>
                    

                ))
            }




        </View>





        <Text style={{fontSize:12, left:45,top:540,position:'absolute'}} >Tel. 618-234-65-44 </Text>
        <Text style={{fontSize:12, left:45,top:520,position:'absolute'}}  >Ubicación: Frente al colegio REX</Text>


    </Page>

</Document>

  )
}
