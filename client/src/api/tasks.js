import axios from './axios'

export const getTasksRequest=() => axios.get('/tasks')

export const getTaskRequest = (id) => axios.get(`/tasks/${id}`)

export const getTaskPlacaRequest = (id) => axios.get(`/taskCarro/${id}`)

export const createTaskRequest= task => axios.post('/savevehicle',task)

export const updateTaskRequest = (id,task) => axios.put(`/updatevehicle/${id}`,task)

export const deleteTaskRequest = (id) => axios.delete(`/deletevehicle/${id}`)