import axios from "./axios.js";

// const API = 'http://10.11.60.124:3999/api'

export const registerRequest=user => axios.post(`/register`,user)

export const loginRequest= user => axios.post(`/login`,user)

export const verifyTokenRequest=() => axios.get('/verify')