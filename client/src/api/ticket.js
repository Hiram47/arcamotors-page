import axios from './axios'

export const getTicketRequest = (id) => axios.get(`/ticket/${id}`)

export const getRowToPayRequest = (id) => axios.get(`/pay/${id}`)

export const createTicketRequest= (tickets) => axios.post('/ticket',tickets)

export const deleteTicketrequest= (id) => axios.delete(`/deleteticket/${id}`)

//new
export const updatePaymentrequest= (pagos,id) => axios.put(`/payment/${id}`,{pagos})

export const getReporteByvehiculoRequest= (ticket) => axios.post(`/fechavehiculo`,ticket)

export const getReporteAllVehiculesRequest= (ticket) => axios.post('/fechareporte',ticket)

export const getPuestoRequest = () => axios.get('/puesto')

//new month

export const getNextMonthRequest = (inicial) => axios.post(`/month`,inicial)

export const getTaxByDayRequest = (inicial) => axios.post(`/interesDay`,{inicial})

// new report
export const getReportByCarRequest = (inicial) => axios.post(`/reportByCar`,inicial)

//new gastos
export const creategastosRequest = (gastos) =>axios.post('/gastos',gastos)

export const getGastosMonthlyRequest = () => axios.get('/currentGastos')

export const deleteGastorequest= (id) => axios.delete(`/deleteGasto/${id}`)

export const getGastosFechaRequest=(fecha) => axios.post('/gastosFecha',fecha)

export const getTotalGastosRequest = () => axios.get('/totalGastos')
