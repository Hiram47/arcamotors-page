import React, { useEffect } from 'react'
import { useForm } from 'react-hook-form'
import { useNavigate, useParams } from 'react-router-dom'
import { useAuth } from '../context/AuthContext'


import { useTasks } from '../context/TaskContext'

const TaskFormPage = () => {

    const {register,handleSubmit,setValue}=useForm()

    const navigate = useNavigate()
    const params=useParams()
    console.log(params.id);

    const {createTask,getTask,updateTask}=useTasks()
   
    const {user}=useAuth()
    console.log(user);
    ///
    // useEffect(() => {
    //     async function loadmonth(inicio){

    //         const calculo_meses= await getNextMonthRequest(inicio)
    //         console.log(calculo_meses);


    //     }
      
    // }, [])

    // loadmonth()

    ///

    useEffect(()=>{

        async function loadTask (){

            if (params.id) {
                const task=await getTask(params.id)
                // const calculo_meses= await getNextMonthRequest(inicio)
                console.log(task);
                setValue('vehiculo',task.vehiculo)
                setValue('folio',task.folio)
                setValue('modelo',task.modelo)
                setValue('color',task.color)
                setValue('costo',task.costo)
                setValue('venta',task.venta)
                setValue('cliente',task.cliente)
                // setValue('serie',task.serie)
                // setValue('placas',task.placas)
                // setValue('inventario',task.inventario)     
                setValue('usuario_modifico',user.email) 
            }else{
                setValue('id_usuario',user.email)           
            }
        }
        loadTask()

    },[])


    const onSubmit= handleSubmit(async (data) =>{

        if (params.id) {
             await updateTask(params.id,data)
        }else{

            await createTask(data);      
        }
         navigate('/vehiculo')
        
    })

  


  return (
    <div className='bg-zinc-800  w-full p-10 rounded-md'>
        <form  onSubmit={onSubmit}>
            {
                params.id ?
                <>
                    <h2 className='text-4xl p-1'>Agregar información</h2>
                    <hr className='py-2' />
                </>
                :
                <>
                    <h2 className='text-4xl p-1'>Agregar vehículo</h2>
                    <hr className='py-2' />
                </>
            }
            <div className='grid grid-cols-4 gap-4'>

            
            <div>

                <label htmlFor="">Vehiculo</label>
                 <input className='w-full bg-zinc-700 text-white px-4 py-2 my-2 rounded-md' type="text" placeholder='Vehiculo' {...register('vehiculo')} autoFocus />
            </div>

            <div>

                <label htmlFor="">Folio</label>
                <input className='w-full bg-zinc-700 text-white px-4 py-2 my-2 rounded-md' type="text" placeholder='Folio' {...register('folio')}  />
            </div>

            <div>

            <label htmlFor="">Modelo</label>
            <input className='w-full bg-zinc-700 text-white px-4 py-2 my-2 rounded-md' type="text" placeholder='Modelo' {...register('modelo')}  />
            </div>

            <div>

                <label htmlFor="">Color</label>
                <input className='w-full bg-zinc-700 text-white px-4 py-2 my-2 rounded-md' type="text" placeholder='Color' {...register('color')}  />
            </div>


            <div>

                <label htmlFor="">Costo</label>
                <input className='w-full bg-zinc-700 text-white px-4 py-2 my-2 rounded-md' type="text" placeholder='Costo' {...register('costo')}  />
            </div>

            <div>

            <label htmlFor="">Venta</label>
            <input className='w-full bg-zinc-700 text-white px-4 py-2 my-2 rounded-md' type="text" placeholder='Venta' {...register('venta')}  />
            </div>

            {
                params.id &&
                
                
                <>
            <div>

                <label htmlFor="">Cliente</label>
                <input className='w-full bg-zinc-700 text-white px-4 py-2 my-2 rounded-md' type="text" placeholder='Cliente' {...register('cliente')}  />
            </div>
                
                
            {/* <div>

            <label htmlFor="">Enganche</label>
            <input className='w-full bg-zinc-700 text-white px-4 py-2 my-2 rounded-md' type="text" placeholder='Enganche' {...register('enganche')}  />
            </div>


            <div>

            <label htmlFor="">Meses</label>
            <input className='w-full bg-zinc-700 text-white px-4 py-2 my-2 rounded-md' type="text" placeholder='Meses' {...register('meses')}  />
            </div>


            <div>

            <label htmlFor="">Interes</label>
            <input className='w-full bg-zinc-700 text-white px-4 py-2 my-2 rounded-md' type="text" placeholder='Interes' {...register('interes')}  />
            </div>

            <div>

            <label htmlFor="">Mensualidad</label>
            <input className='w-full bg-zinc-700 text-white px-4 py-2 my-2 rounded-md' type="text" placeholder='Mensualidad' {...register('mensualidad')}  />
            </div> */}


            {/* <div>

            <label htmlFor="">Pagos</label>
            <input className='w-full bg-zinc-700 text-white px-4 py-2 my-2 rounded-md' type="text" placeholder='Pagos' {...register('inventario')}  />
            </div> */}
                </>
                
            }
            {/* 







            <div> */}

            {/* <label htmlFor="">Status</label> */}
            {/* <input className='w-full bg-zinc-700 text-white px-4 py-2 my-2 rounded-md' type="hidden" placeholder='Status' {...register('inventario')}  /> */}
            {/* </div> */}

            {/* <input className='w-full bg-zinc-700 text-white px-4 py-2 my-2 rounded-md' type="hidden" placeholder='Usuario' {...register('id_usuario')}  />
            <input className='w-full bg-zinc-700 text-white px-4 py-2 my-2 rounded-md' type="hidden" placeholder='Modifico' {...register('usuario_modifico')}  /> */}
        </div>
            <button className='bg-red-600 hover:bg-red-700 px-3 py-3 my-5 rounded-md w-full transition duration-700 ease-in-out'>GUARDAR</button>
        </form>
    </div>
  )
}

export default TaskFormPage