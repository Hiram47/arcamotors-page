import React, { useEffect, useState } from 'react'
import { useForm } from 'react-hook-form'
import { useNavigate, useParams } from 'react-router-dom'
import { useTicket } from '../context/TicketContext'
import { Toaster, toast } from 'sonner';
import { HistoryTable } from '../components/HistoryTable';
import { PDFHistorialPagos } from '../pdf/PDFHistorialPagos';
import { PDFViewer } from '@react-pdf/renderer';

const PagarPage = () => {
  const {register,setValue,handleSubmit,getValues}=useForm()

  const {getTickets,getReporteCar,historyCar,tax,getTaxByDay,datarow, getRowTicket,updatePay, getmonthContext,nextmonth}= useTicket()
  const [load, setload] = useState(true)
  const [loadtax,setloadtax] = useState(true)

  const [Counter, setCounter] = useState(0)

  const navigate = useNavigate()

  
  const {id}=useParams()

  useEffect(() => {
    console.log('useeffect');
    async function loadpago(){
      console.log(datarow.fecha_registro);
      // setValue('venta2',pagos.diferencia)
      await getRowTicket(id)
      // const calculo_meses= await getmonthContext({inicial:'2023-11-01',pagos:3})

      const calculo_meses= await getmonthContext({inicial:datarow.fecha_mensualidad,pagos:Counter>Number(datarow.pagos)?Counter:Number(datarow.pagos)})
      console.log(calculo_meses);
      console.log(nextmonth);

      const porcentaje=datarow.mensualidad *.10

   

      console.log(datarow);
      setValue('venta2',datarow.venta2)
      setValue('enganche',datarow.enganche)
      setValue('meses',datarow.meses)
      setValue('interes',datarow.interes)
      setValue('mensualidad',datarow.mensualidad)
      setValue('pagos',Counter>Number(datarow.pagos)?Counter:Number(datarow.pagos))
      setValue('fecha_registro',datarow.fecha_registro)
      setValue('fecha_mensualidad',datarow.fecha_mensualidad)

      // setValue('fecha_registro',datarow.mensualidad)
      setload(false)
      console.log(datarow.id);

      // setTimeout(async () => {
        const history= await getReporteCar({id:datarow.id,id_vehiculo:datarow.id_vehiculo})
        
        console.log(history);
      // }, 200);
      
      console.log(historyCar);
      
      
      console.log(datarow.fecha_mensualidad);
    }
    loadpago()
  }, [load,Counter])
  

  useEffect(() => {

    async function loadTax(){
             const calculoInteres= await getTaxByDay(nextmonth)
           
          
             setTimeout(() => {
              
               console.log(calculoInteres);
               console.log(tax.dias_transcurridos);
               setloadtax(false)
             }, 1000);

    }
    loadTax()
   
  }, [nextmonth,loadtax])
  
  

  const onSubmit= handleSubmit(async pagos=>{
    // const confirmar=confirm('Confirmacion de cobro:')
    // if (confirmar==true) {

    toast('Estas seguro de hacer el cobro?',{
      action:{
        label:'Aceptar',
        onClick:async()=>{

          if (pagos.pagos >=datarow.meses){
            alert('Ya termino todas las mensualidades')
      
          }else{
      
            setCounter(Number(pagos.pagos)+1)
            console.log(Number(pagos.pagos)+1);
            const pay=Number(pagos.pagos)+1
            await updatePay(id,pay)
            console.log(Counter);
           
              navigate(-1)
              
            
            toast.success('Cobro realizado correctamente')
          


          }
        },
        
      },
      cancel:{
        label:'Cancelar',
        onClick: () => console.log('Cancelado')

    }


      
    })
    
      
    // }
    return

    // console.log(...pagos,);
  })

  const onrembolsar=async()=>{
    // const confirmar=confirm('Confirmacion de reembolso:')
    // if (confirmar==true) {

      // if (Counter<=0) {
      //   alert('Error es inferior al pago numero 0')
        
      // }
      toast('Estas seguro de Hacer un reembolso?',{
        action:{
          label:'Aceptar',
          onClick:async()=>{

            await updatePay(id,datarow.pagos-1)
            
              
              setCounter(datarow.pagos-1)
           
            // setCounter(Counter-1)
            console.log(Counter);
            console.log(datarow.pagos-1);
            toast.success('Rembolso realizado correctamente')
          }

        },
        cancel:{
          label:'Cancelar',
                onClick: () => console.log('Cancelado')

        }


      })

      
     
    // }
    return

    // console.log(...pagos,);  

  }

  console.log(id);
  return (
    <div className='bg-zinc-800  w-full p-10 rounded-md'>
          <h2 className='text-3xl  '>Mensualidad</h2>
          <hr className='py-5' />
      <form onSubmit={onSubmit}>
        <div className='grid sm:grid-cols-2 lg:grid-cols-4   gap-4 ' >

        <div>
            <label htmlFor="">Precio</label>
            <input className=' bg-zinc-700 rounded-md p-2 mx-2 w-full' type="text" placeholder='Venta' {...register('venta2')}  required  />
           </div>

           <div>
           <label htmlFor="">Enganche</label>
            <input className=' bg-zinc-700 rounded-md p-2 mx-2 w-full' type="text" placeholder='Enganche' {...register('enganche')} autoFocus   />
           </div>

           <div>
            <label htmlFor="">Meses</label>
            <input className=' bg-zinc-700 rounded-md p-2 mx-2 w-full' type="text" placeholder='Meses' {...register('meses')} autoFocus   />
            </div>

            <div>
            <label htmlFor="">Interes</label>
            <input className=' bg-zinc-700 rounded-md p-2 mx-2 w-full' type="text" placeholder='Interes' {...register('interes')} autoFocus  />
            </div>
            <div>
            <label htmlFor="">Mensualidad</label>
            <input className=' bg-zinc-700 rounded-md p-2 mx-2 w-full' type="text" placeholder='Mensualidad' {...register('mensualidad')} autoFocus  />
            </div>

            <div>
            <label htmlFor="">Pagos Realizados</label>
            <input className=' bg-zinc-700 rounded-md p-2 mx-2 w-full' type="text" placeholder='Pagos' {...register('pagos')} autoFocus  />
            </div>

            <div>
            <label htmlFor="">Fecha registro</label>
            <input className=' bg-zinc-700 rounded-md p-2 mx-2 w-full' type="text" placeholder='Pagos' {...register('fecha_registro')} autoFocus  />
            </div>
            <div>
            <label htmlFor="">Fecha mensualidad</label>
            <input className=' bg-zinc-700 rounded-md p-2 mx-2 w-full' type="text" placeholder='Pagos' {...register('fecha_mensualidad')} autoFocus  />
            </div>

            {/* {
              tax.dias_transcurridos>=0&& */}
              <>

              <div>

              <label htmlFor="">Siguiente mensualidad</label>
              <h2>{new Date(nextmonth).toLocaleDateString("sv-SE")}</h2>
            </div>


            <div>
              <label htmlFor="">Dias transcurridos</label>
              {
                tax.dias_transcurridos >=0 
                ?
                <h2 style={{color:'red'}}>{tax.dias_transcurridos}</h2>
                :
                <h2 style={{color:'green'}}>{tax.dias_transcurridos}</h2>
              }
            </div>

            <div>
              <label htmlFor="">Calculo por dia</label>
              <h2>{new Intl.NumberFormat().format(((datarow.mensualidad *.10)/30)) }</h2>
            </div>
            <div>
              <label htmlFor="">Calculo por dias transcurridos</label>
              <h2>{new Intl.NumberFormat().format(((datarow.mensualidad *.10)/30) *(tax.dias_transcurridos))}</h2>
            </div>

            <div>
              <label htmlFor="">Calculo de interes mensualidad</label>
              {
                tax.dias_transcurridos >=0
                ?

                <h2>{new Intl.NumberFormat().format(((((datarow.mensualidad *.10)/30) *(tax.dias_transcurridos))+(datarow.mensualidad)))}</h2>
                  :
                <h3>NO EXISTEN DIAS TRANSCURRIDOS</h3>
              }
            </div>

            


          </>
          {/* } */}
        </div>
            <div className='mb-3'>
                <button className=' font-bold w-full bg-green-800 hover:bg-green-700 py-2 px-1 my-3 rounded-md'>Cobrar</button>

            </div>

            {/* <div className='mb-3 w-full'>
                <span onClick={onrembolsar} className=' font-bold w-full bg-red-900 hover:bg-red-700 py-2 px-1 my-3 rounded-md'>Reembolsar cobro</span>

            </div> */}

      </form>
            <h2 className=' text-2xl'>Historial de mensualidades</h2>
            <hr className='py-3' />

            <div className='relative overflow-x-auto shadow-md sm:rounded-lg'>

<table  className= 'w-full text-sm text-left text-white dark:text-white '
// className= ' bg-zinc-800 max-w-md w-full p-10 rounded-lg    min-w-full text-center text-md font-light  '
>
    <thead className='text-xs text-white uppercase bg-gray-50 dark:bg-red-700 dark:text-white-400 dark:text-white-400'
    // className='border-b bg-red-900 font-medium dark:border-neutral-500 dark:text-white'
    >
    <tr>
      <th 
      scope="col" className='px-6 py-3'
      // className='col-md-1'
      >#</th>

      <th scope="col" className='px-6 py-3'>precio</th>
      <th scope="col" className='px-6 py-3'>Pagado</th>
      <th scope="col" className='px-6 py-3'>Faltante</th>
      <th scope="col" className='px-6 py-3'>Meses</th>
      <th scope="col" className='px-6 py-3'>interes %</th>
      <th scope="col" className='px-6 py-3'>Mensualidad</th>
      <th scope="col" className='px-6 py-3'>Pagos</th>
      <th scope="col" className='px-6 py-3'>Fecha</th>
      {/* <th className='col-md-1'>Serie</th>
      <th className='col-md-1'>Placas</th>
    <th className='col-md-1'>Inventario</th> */}
      
      {/* <th>Reporte</th> */}
    </tr  >

    </thead>
    <tbody>

         {
          // console.log(historyCar)
          historyCar.map((history,i)=>(

            <HistoryTable history={history} i={i} key={history.id} />

          ))
        //  tasks.map((task,i)=>  (

        //         <TaskTable task={task} i={i} key={task.id} />
        //     ) )
            
            }

    </tbody>

</table>

  {
   
      
    // <PDFViewer className='py-5' style={{height:'70vh',width:'50%'}}>

    //     <PDFHistorialPagos history={historyCar}  />

    // </PDFViewer>
   
  }


    </div>

            {
              //there is a logic from table
              // <HistoryTable/>
               
            }

      <Toaster richColors position='top-center'/>
    </div>
  )
}

export default PagarPage