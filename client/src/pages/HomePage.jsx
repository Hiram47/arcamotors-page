import React from 'react'

import arca from '../assets/arca.png'
import ferrari from '../assets/ferrari.png'
import audi from '../assets/audi.png'
import toyota from '../assets/toyota.png'
import mz from '../assets/mercedez.png'
import vw from '../assets/vw.png'
import honda from '../assets/honda.png'
import ford from '../assets/ford2.png'
import chev from '../assets/chev.png'
import kia from '../assets/kia.png'
const HomePage = () => {
  return (
    <>
      <div className='grid sm:grid-cols-2 md:grid-cols-8   gap-4 '>
        <img className='py-2' src={audi} alt="" style={{width:"100px"}} />
        <img className='' src={toyota} alt="" style={{width:"120px"}} />
        <img className='' src={mz} alt="" style={{width:"120px"}} />
        <img className='' src={vw} alt="" style={{width:"120px"}} />
        <img className='' src={honda} alt="" style={{width:"120px"}} />
        <img className='' src={ford} alt="" style={{width:"120px"}} />
        <img className='' src={chev} alt="" style={{width:"120px"}} />
        <img className='' src={kia} alt="" style={{width:"120px"}} />

      </div>
    <div className='flex h-[calc(100vh-100px)] items-center justify-center'>
      {/* <h2 className='text-4xl'>Secretaria de Seguridad Pública</h2> */}
      
      <div>

      <img className='' src={arca} alt="" style={{width:"520px"}} />
      {/* <img className='' src={ferrari} alt="" style={{width:"600px"}} /> */}
      <h2 className='text-5xl text-center font-display'>SEMINUEVOS</h2>
      <hr className='' />
      <h2 className='text-4xl text-center text-red-800 font-display'>DISTINGUIDOS POR EXCELENCIA</h2>

      </div>

    </div>
    </>
  )
}

export default HomePage