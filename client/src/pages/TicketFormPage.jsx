import React, { useEffect, useState } from 'react'
import { useForm } from 'react-hook-form'
import { Link, useParams } from 'react-router-dom'
import { useAuth } from '../context/AuthContext'
import { useTicket } from '../context/TicketContext'

import numeral from 'numeral';

import {PDFViewer} from '@react-pdf/renderer';

import {NumericFormat} from 'react-number-format';

import { Toaster, toast } from 'sonner';
import dayjs from 'dayjs'
import utc from 'dayjs/plugin/utc'
import {es} from 'dayjs/locale/es-mx'
import { useTasks } from '../context/TaskContext'
import { PDFHistorial } from '../pdf/PDFHistorial'
dayjs.locale('es')
dayjs.extend(utc)

export const TicketFormPage = () => {
    const params=useParams()
     const {user}=useAuth()
    const {register,setValue,handleSubmit,getValues}=useForm()
    const {prueba,getTickets,tikets,createTiketGas,deleteTicketGas}=useTicket()
    const {getTask} = useTasks()
    const [load, setload] = useState(true)

    const [counter, setcounter] = useState(0)
    const [diference, setdiference] = useState(0)

    const [datosCarro, setdatosCarro] = useState({})

    const [carga, setcarga] = useState(true)

    //new

    useEffect(() => {
        setTimeout(() => {

            console.log('useefect working');
            console.log(datosCarro);
            console.log(tikets[0]?.diferencia);
                
            // setValue('venta2', tikets[0]==undefined||tikets[0]==null?datosCarro.venta:tikets[0].diferencia)
            setValue('venta2', tikets[0]==undefined||tikets[0]==null?datosCarro.venta:(tikets[0].meses.toFixed(2)-tikets[0].pagos.toFixed(2))*(tikets[0].venta2.toFixed(2)-tikets[0].enganche.toFixed(2))/tikets[0].meses)

            setcarga(false)
        }, 100);
        // setValue()
     
    }, [carga,tikets])
    

    // setTimeout(() => {
        
    //     setValue('venta2',tikets[0]?.enganche?datavehiculo.venta: tikets[0]?.venta2-tikets[0]?.enganche)
                
    // }, 200);

//     useEffect(() => {

//         async function loadVenta(){
// setTimeout(() => {
    
//     tikets

//     console.log(tikets[0]);
//     console.log(tikets[0].diferencia);
//     setcarga(false)
// }, 200);
//         }
//         loadVenta()
   
//     }, [])
    

    useEffect(() => {
        //cargar datos del carro
        async function loadIdvehiculo(){
            console.log(params.id,'datos del carro');

            const datavehiculo=await getTask(params.id)
            setdatosCarro(datavehiculo)
            setValue('venta2', tikets[0]==undefined?datosCarro.venta:tikets[0].diferencia)

//new
            // const datosdiferencia = await getTickets(params.id)
            // console.log(datosdiferencia);

            console.log(datavehiculo);
            console.log(getValues());
            setTimeout(() => {
                //  setValue('venta2',datavehiculo.venta- datavehiculo.enganche)


                //setValue('venta2',datavehiculo.diferencia)
                
            //     setValue('venta2', tikets[0].venta2)
            setcarga(false)
            }, 200);
            
            // setValue('enganche',datavehiculo.enganche)
        }

        loadIdvehiculo()

     
    }, [])
    

    //

    

    useEffect(() => {
        console.log(params.id);
        let today = new Date().toLocaleDateString("sv-SE")
        
        async function loadticket(){
           const getallTickets=await getTickets(params.id)
           console.log(getallTickets);
           setValue('id_vehiculo',params.id)
           setValue('usuario_modifico',user.email)
           setValue('fecha_registro',today)
           setValue('fecha_mensualidad',today)

        }
        loadticket()
   
    }, [load])//para cargar la tabla al ingresar un dato del formulario
   
    

    

    const onSubmit =handleSubmit(async tickets =>{
        const {mensualidad}=getValues()
        console.log(mensualidad);

        if (mensualidad==undefined|null|0) {
            console.log('vacio');
            toast('Es necesario calcular la mensualidad primero')
            return ;

            
        }else{

            const res= await createTiketGas(tickets)
            console.log(tickets);
            console.log(res);
            
            //cargar datos de la tabla
            setload(false)
        }


    })

    const ondelete = async( id)=>{
        // const confirmar=confirm('Confirmacion de cobro son:')

        toast('Estas seguro de eliminar el registro?',{
            action:{
                label:'Aceptar',
                onClick:async()=>{

                    await deleteTicketGas(id)
                    console.log(id);
                    setload(false)
                }
            },
            cancel:{
                label:'Cancelar',
                onClick: () => console.log('Cancelado')

            }
        })

        //if (confirmar == true){

      //  }

    }

    const onCobrar=async()=>{

        const confirmar=confirm('Confirmacion de cobro son:')

        if (confirmar == true){
            setcounter(counter+1)

            console.log(counter);
        }

        return;
        // await updatePago


    }

    const calcularCotizacion =()=>{
        console.log('hola333');
        const {venta2,meses,interes,enganche}= getValues()
        console.log(venta2,meses,interes,enganche);

        
            
            
            // const diferencia = venta2-enganche
            setdiference(venta2-enganche)
            const interestotal=interes/1000
            const operacion1 = interestotal*meses
            const operacion2= diference *(operacion1+1)/meses
            console.log(Number(operacion2.toFixed(2)));
            setValue('mensualidad',Number(operacion2.toFixed(2)))
       

    }
    
  return (
    <div className='bg-zinc-800  w-full p-10 rounded-md '>
           
        <form onSubmit={onSubmit}>
            {
            tikets[0] ?<h2 className='text-4xl' >Pago a capital</h2> :<h2 className='text-4xl' >Mensualidad</h2>
            }
            <hr className='py-2 '  />
            <div className='grid sm:grid-cols-2 md:grid-cols-4   gap-4 '>
            
            <input className=' bg-zinc-700 rounded-md p-2 mx-2 ' type="hidden" placeholder='idVehiculo' {...register('id_vehiculo')}  />
            
           

            <input className=' bg-zinc-700 rounded-md p-2 mx-2' type="hidden" placeholder='Usuario modifico' {...register('usuario_modifico')} autoFocus   />
           <div>
            <label htmlFor="">Precio</label>
            <input  className=' bg-zinc-700 rounded-md p-2 mx-2 w-full' type="text" aria-describedby="price-currency" placeholder='Venta' {...register('venta2')} 
            // onChange={e=>{
            //     const value = e.target.value; 
            //     console.log(value);
            //     const formattedValue = numeral(value).format('$0,0.00');
            //     e.target.value = formattedValue;
                // }} 
                disabled required  />


            {/* <NumericFormat
        thousandSeparator=","
        decimalSeparator="."
        prefix="$"
        decimalScale={2}
        // value={...register('venta2')}
        {...register('venta2')}
      /> */}
          

            {/* <NumericFormat
            thousandSeparator=","
            decimalSeparator='.'
            prefix='$'
            customInput={inputProps => (
                <input
                  {...inputProps}
                   className=' bg-zinc-700 rounded-md p-2 mx-2 w-full' type="text" aria-describedby="price-currency" placeholder='Venta'
                  
                />
              )}
              {...register('venta2')}
            
            /> */}
           </div>
           
           <div>
           <label htmlFor="">Enganche</label>
            <input className=' bg-zinc-700 rounded-md p-2 mx-2 w-full' type="text" placeholder='Enganche' {...register('enganche')} autoFocus   />
           </div>

            <div>
            <label htmlFor="">Meses</label>
            <input className=' bg-zinc-700 rounded-md p-2 mx-2 w-full' type="text" placeholder='Meses' {...register('meses')} autoFocus   />
            </div>

            <div>
            <label htmlFor="">Interes%</label>
            <input className=' bg-zinc-700 rounded-md p-2 mx-2 w-full' type="text" placeholder='Interes' {...register('interes')} autoFocus   />
            </div>

            <div>
            <label htmlFor="">Mensualidad</label>
            <input className=' bg-zinc-700 rounded-md p-2 mx-2 w-full' type="text" placeholder='Mensualidad' {...register('mensualidad')} required disabled  />
            </div>
            {/* <button className='bg-slate-900 rounded-md' onClick={prueba2} >calcular</button> */}


                {/* <input className=' bg-zinc-700 rounded-md py-2 mx-2 ' type="date" placeholder='fecha registro' {...register('fecha')} required/>
                <input className=' bg-zinc-700 rounded-md p-2 mx-2' type="text" placeholder='Litros' {...register('litros')} autoFocus   />
                <input className=' bg-zinc-700 rounded-md p-2 mx-2' type="text" placeholder='Costo' {...register('costo')} />
                <input className=' bg-zinc-700 rounded-md p-2 mx-2' type="text" placeholder='Km'{...register('km_final')} />
                <input className=' bg-zinc-700 rounded-md p-2 mx-2' type="text" placeholder='No Ticket' {...register('no_ticket')} />
                <input className=' bg-zinc-700 rounded-md p-2 mx-2' type="text" placeholder='Lugar de comision' {...register('comision')} />
                
                <input className=' bg-zinc-700 rounded-md p-2 mx-2' type="text" placeholder='usuario' {...register('id_usuario')}  />
                <input className=' bg-zinc-700 rounded-md p-2 mx-2' type="hidden" placeholder='fecha registro' {...register('fecha_registro')}  />
            <input className='bg-zinc-700 rounded-md p-2 mx-2' type="text" placeholder='observacion' {...register('observaciones')}/> */}

            </div>
            <div className='mb-3 py-5'>
            <span className=' font-bold py-2 px-1 my-3 w-full bg-teal-600 rounded-md text-center text-lg' onClick={calcularCotizacion} >Calcular mensualidad</span>

            </div>
            <div className='mb-3'>
                <button className=' font-bold w-full bg-red-600 hover:bg-red-800 py-2 px-1 my-3 rounded-md'>Guardar</button>

            </div>
            <div className='mb-6'>
                {/* <input className=' bg-zinc-700 rounded-md p-2 mx-2' type="text" /> */}
            </div>

        </form>

        <div className='relative overflow-x-auto shadow-md sm:rounded-lg'>



        <table className= 'w-full text-sm text-left text-white dark:text-white '>
            <thead className='text-xs text-white uppercase bg-gray-50 dark:bg-red-700 dark:text-white-400 dark:text-white-400'>
            <tr>
              <th scope="col" className='px-6 py-3'>#</th>

              <th scope="col" className='px-6 py-3'>Precio</th>
              <th scope="col" className='px-6 py-3'>Enganche</th>
              <th scope="col" className='px-6 py-3'>Meses</th>
              <th scope="col" className='px-6 py-3'>Mensualidad</th>
              <th scope="col" className='px-6 py-3'>Pagos realizados</th>
              <th scope="col" className='px-6 py-3'>Pagos faltantes</th>
              {/* <th className='col-md-1'>No.ticket</th>
              <th className='col-md-1'>Comision</th>
              <th className='col-md-1'>Observaciones</th>
              <th className='col-md-1'>Usuario</th> */}
              <th scope="col" className='px-6 py-3'>Calculo sin interes</th>
              <th scope="col" className='px-6 py-3'>Venta sin interes</th>
              <th scope="col" className='px-6 py-3'>Venta con interes (total)</th>
              <th scope="col" className='px-6 py-3'>Cobrar</th>
              <th scope="col" className='px-6 py-3'>Eliminar</th>
             
            </tr >

            </thead>
            <tbody>
                {
                    console.log(tikets[0])
                }


             
        {
            
            tikets.map((tiket,i)=>(
                
                <tr key={tiket.id} className='bg-white border-b dark:bg-zinc-800 dark:border-gray-800 hover:bg-gray-50 dark:hover:bg-gray-600' >
                    <td className='px-2 py-2'>{i+1}</td>
                    {/* <td>{dayjs.utc(tiket.fecha ).locale('es').format('DD/MM/YYYY')}</td>
                    <td>{tiket.litros} lts.</td>
                    <td>${tiket.costo?.toUpperCase()}</td>
                    <td>{tiket.km_final} KM</td>
                    <td>{tiket.no_ticket}</td>
                    <td>{tiket.comision?.toUpperCase()}</td>
                    <td>{tiket.observaciones?.toUpperCase()}</td>
                <td>{tiket.id_usuario?.toUpperCase()}</td> */}
                    <td className='px- py-1'>$ {new Intl.NumberFormat().format(tiket.venta2)}</td>
                    <td className='px-2 py-2'>$ {new Intl.NumberFormat().format(tiket.enganche)}</td>
                    <td className='px-2 py-2'> {new Intl.NumberFormat().format(tiket.meses)}</td>
                    <td className='px-2 py-2'> ${new Intl.NumberFormat().format(tiket.mensualidad.toFixed(2))}</td>
                    <td className='px-2 py-2'>{new Intl.NumberFormat().format(tiket.pagos)} </td>
                    <td className='px-2 py-2'>{new Intl.NumberFormat().format(tiket.meses-tiket.pagos)} </td>

                    <td className='px-2 py-2'>${new Intl.NumberFormat().format((tiket.venta2-tiket.enganche)/tiket.meses)} </td>

                    <td className='px-2 py-2'>${new Intl.NumberFormat().format((tiket.meses.toFixed(2)-tiket.pagos.toFixed(2))*(tiket.venta2.toFixed(2)-tiket.enganche.toFixed(2))/tiket.meses.toFixed(2))} </td>
                    <td className='px-2 py-2'>$ {new Intl.NumberFormat().format(tiket.mensualidad*tiket.meses)}</td>

                    


                    {/* <td> ${new Intl.NumberFormat().format((new Intl.NumberFormat().format(tiket.meses)-counter)*(Number(tiket.venta2) - Number(tiket.enganche)))}</td> */}


                    {/* <td><span className='bg-green-700 hover:bg-green-600 p-1 m-2 rounded-md font-bold' onClick={onCobrar}>Cobrar</span></td> */}

                    {
                        i ==0 &&<>
                        <td><Link className='bg-green-600 rounded-md min-w-full text-white  font-bold hover:bg-green-700 p-2 ' to={`/mensualidad/${tiket.id}`} >Cobrar</Link></td>
                    <td><button className='bg-red-800 hover:bg-red-600 p-1 m-2 rounded-md font-bold' onClick={()=>ondelete(tiket.id)}>Eliminar</button></td>
                        </>
                    }
                </tr>
            ))
        }

            </tbody>

        </table>
        </div>

        <div className='py-10 '>
            {
                tikets[0] ?
                <PDFViewer style={{height:'70vh',width:'50%'}}>

                    <PDFHistorial tickets={tikets}  />

                </PDFViewer >
                :
                <>
                SIN REGISTROS
                </>
            }

        </div>

        

        {/* <iframe src="https://docs.google.com/forms/d/e/1FAIpQLScKgKddgFK_uciJi72ukZcMO22L8MyxXpl2q0GSRjBeimIHmg/viewform?embedded=true" width="640" height="612" frameborder="0" marginheight="0" marginwidth="0">Cargando…</iframe> */}
        <Toaster richColors theme='dark' toastOptions={{style:{background: 'red'}}}  position="top-center" />
    </div>
  )
}
