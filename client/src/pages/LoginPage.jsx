import React, { useEffect } from 'react'
import { useForm } from 'react-hook-form'
import { useAuth } from '../context/AuthContext'
import { Link, useNavigate } from 'react-router-dom'

const LoginPage = () => {

    const {register,handleSubmit,formState:{errors}}=useForm()

    const {signin,errors:loginErrors,isAuthenticaded}= useAuth()

    const navigate=useNavigate()

    const onSubmit = handleSubmit(data =>{
        console.log(data);
        signin(data)
    })

    useEffect(() => {
        if (isAuthenticaded) {
            navigate('/vehiculo')
        }
    
    }, [isAuthenticaded])
    

  return (
    <div className='flex h-[calc(100vh-100px)] items-center justify-center'>
        <div className='bg-zinc-800 max-w-md w-full p-10 rounded-md' >
        {
            loginErrors.map((error,i) =>(
                <div key={i} className='bg-red-500 p-2 text-white text-center my-2' >{error}</div>
            ))
        }

          <form onSubmit={onSubmit} >
            <h2 className=' text-3xl text-white font-bold mb-5'>Login</h2>
         


            <input className='w-full bg-zinc-700 text-white px-4 py-2 rounded-md my-2' type="email" placeholder='Email' {...register('email',{required:true}  )} />
            {   errors.email &&(<p className='text-red-500' >Email is required</p> ) }


            <input className='w-full bg-zinc-700 text-white px-4 py-2 rounded-md my-2' type="password" placeholder='Password'  {...register('password',{required:true}  )}   />
            {   errors.password &&(<p className='text-red-500' >Password is required</p> ) }



            <button className='bg-sky-600 px-4 py-2 rounded-md hover:bg-sky-500' type='submit' >Login</button>
        </form>

        <p className='flex gap-x-2 justify-between'>
            Don't have an account? <Link to='/register' className='text-sky-500'>Sign up</Link>
        </p>
        </div>
    </div>
  )
}

export default LoginPage