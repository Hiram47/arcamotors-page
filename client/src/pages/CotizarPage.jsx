import React, { useEffect, useState } from 'react'
import { useForm } from 'react-hook-form'
import { useNavigate, useParams } from 'react-router-dom'
import { useAuth } from '../context/AuthContext'

import { useTasks } from '../context/TaskContext'
import { PDFViewer } from '@react-pdf/renderer'
import {PDFCotizar} from '../pdf/PDFCotizar'
import { Toaster, toast } from 'sonner';

export const CotizarPage = () => {

    const {register,handleSubmit,setValue}=useForm()

    const navigate = useNavigate()
    // const params=useParams()
    // console.log(params.id);

    // const {createTask,getTask,updateTask}=useTasks()
   
    const {user}=useAuth()
    console.log(user);

    const [datavehiculo, setdatavehiculo] = useState({})






    const onSubmit= handleSubmit(async (data) =>{

        
        let date = new Date()
        
        let day = date.getDate()
        let month = date.getMonth() + 1
        let year = date.getFullYear()
        let fulldate=''
        if(month < 10){
            console.log(`${day}-0${month}-${year}`)
            fulldate=`${day}-0${month}-${year}`
        }else{
            console.log(`${day}-${month}-${year}`)
            fulldate= `${day}-${month}-${year}`
        }
        
        
        
        console.log(data);
        console.log(fulldate);
        const {venta,enganche,interes,mensualidad,meses,vehiculo}=data
        if (vehiculo!=''||venta!=""||interes!="") {
            
            const diferencia=venta-enganche
            const interestotal=interes/1000
            const operacion1 = interestotal*meses
            const operacion2= diferencia *(operacion1+1)/meses
    
            setdatavehiculo({venta,enganche,meses,interes,diferencia,interestotal,operacion1,operacion2,vehiculo,fulldate})
            console.log(datavehiculo);
    
            
    
            console.log(Number(operacion2.toFixed(2)));
            setValue('mensualidad',Number(operacion2.toFixed(2)))
        }
        else{
            // alert('campos vacios')
            toast('Llenar todos los campos para poder realizar la cotizacion')
        }


      
        
    })

  

  return (
    <div className='bg-zinc-800  w-full p-10 rounded-md'>
        <form  onSubmit={onSubmit}>
                <h2 className='text-4xl my-1' >Cotización</h2>
                <hr className='my-2' />
            <div className='grid grid-cols-4 gap-4'>

            <div>

                 <label htmlFor="">Vehiculo</label>
                 <input className='w-full bg-zinc-700 text-white px-4 py-2 my-2 rounded-md' type="text" placeholder='Vehiculo' {...register('vehiculo')}  />
            </div>

            <div>

            <label htmlFor="">Precio</label>
            <input className='w-full bg-zinc-700 text-white px-4 py-2 my-2 rounded-md' type="text" placeholder='Precio' {...register('venta')}  />
            </div>
            <div>

                <label htmlFor="">Enganche</label>
                <input className='w-full bg-zinc-700 text-white px-4 py-2 my-2 rounded-md' type="text" placeholder='Enganche' {...register('enganche')}  />
            </div>

            <div>

                <label htmlFor="">Interes</label>
                <input className='w-full bg-zinc-700 text-white px-4 py-2 my-2 rounded-md' type="text" placeholder='Interes' {...register('interes')}  />
            </div>

            
            <div>

                <label htmlFor="">Meses</label>
                 <input className='w-full bg-zinc-700 text-white px-4 py-2 my-2 rounded-md' type="text" placeholder='Meses' {...register('meses')} autoFocus />
            </div>

            <div>

                <label htmlFor="">Mensualidad</label>
                <input className='w-full bg-zinc-700 text-white px-4 py-2 my-2 rounded-md' type="text" placeholder='Mensualidad' {...register('mensualidad')}  />
            </div>

            {/* <div>

            <label htmlFor="">Modelo</label>
            <input className='w-full bg-zinc-700 text-white px-4 py-2 my-2 rounded-md' type="text" placeholder='Modelo' {...register('modelo')}  />
            </div>

            <div>

                <label htmlFor="">Color</label>
                <input className='w-full bg-zinc-700 text-white px-4 py-2 my-2 rounded-md' type="text" placeholder='Color' {...register('color')}  />
            </div> */}



          
        </div>
            <button className='bg-red-700 hover:bg-red-600 px-3 py-3 my-5 rounded-md w-full transition duration-700 ease-in-out'>COTIZAR</button>
        </form>

        <div className='py-10'>
            {datavehiculo.operacion2 !=null ?
            //pdf-lib new libreria

            <PDFViewer  style={{height:'80vh',width:'80%'}}>

                <PDFCotizar  datavehiculo={datavehiculo} user={user}/>

            </PDFViewer>
            :
            <h3>NO PDF</h3>
            }

        </div>
        <Toaster position="top-center" />
    </div>
  )
}
