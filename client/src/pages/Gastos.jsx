import React, { useEffect, useState } from 'react'
import { useForm } from 'react-hook-form'
import { useAuth } from '../context/AuthContext'
import { Toaster, toast } from 'sonner';
import { useTicket } from '../context/TicketContext';
import { TableGastos } from '../components/TableGastos';

export const Gastos = () => {
    const {register,handleSubmit,setValue,getValues}=useForm({mode:'onBlur'})
    const {
        register: register2,
        formState: { errors: errors2 },
        handleSubmit: handleSubmit2,
      } = useForm({
        mode: "onBlur",
      });
    const {createGastosContabilidad,Gastos,getGastosMonthly,getGastosReporte,sumatoria,getTotalGastos}= useTicket()

    let anidado=0
    const {user}=useAuth()
    console.log(user);
    const [gastos, setgastos] = useState({})
    const [load, setload] = useState(false)

    const [fech, setfech] = useState({})

    // useEffect(() => {

    //     setfech(setValue('inicial','2024-01-20'))
    //     console.log(fech);
    
    // }, [])
    
    

    useEffect(() => {
        async function loadGastos() {

            setValue('usuario',user.email)
            await getGastosMonthly()
            gastos
            setload(false)

            await getTotalGastos()
            sumatoria

        }
        loadGastos()
        // console.log(getGastosMonthly());
        
    }, [])

    useEffect(() => {
        async function loadGastos() {

            setValue('usuario',user.email)
            await getGastosMonthly()
            gastos
            setload(false)

            await getTotalGastos()
            sumatoria

        }
        loadGastos()
    
    }, [load])
    
    //console.log(Gastos);
    const onSubmit=handleSubmit(async data =>{
        
        
        // if (data.motivo !='' || data.fecha !='' || data.cantidad !='') {
            
            
                console.log(data);
                console.log(+data.cantidad);
                // console.log('ss'+data.cantidad.length);
                setgastos({...data,usuario:user.email})
           
            
            const saved = await createGastosContabilidad(data)
            console.log(saved);
            setload(true)

            toast('Se ingresaron los datos correctamente')

            
        // }
        // else{

        //     toast.error('Los campos estan vacios')
        //     console.log(data.cantidad);
        // }

        console.log(gastos)

    })

    const onSubmit2= handleSubmit2(async data=>{
        console.log(data);
     //  alert(JSON.stringify(data))

       // const inicial=getValues('inicial')
       // const final=getValues('final')
       // console.log({inicial,final});
       // setValue('inicial',data.inicial)
        // const res=await getGastosReporte({inicial,final})
        const res=await getGastosReporte(data)
        console.log(res);
        setload(false)
    })

    // const onGasto=  (async data =>{
    //     //sconsole.log(data);
    //     const inicial=getValues('inicial')
    //     const final=getValues('final')
    //     console.log({inicial,final});
    //    // setValue('inicial',data.inicial)
    //     const res=await getGastosReporte({inicial,final})
    //     console.log(res);

    // })
  return (
    <div className='bg-zinc-800  w-full p-10 rounded-md'>
        <h2 className='text-4xl py-5' >Gastos</h2>
        <hr className='py-5' />
        
        <form key={1} onSubmit={onSubmit} >
            <div className='grid grid-cols-3 gap-3'>
                <div>

                    <label htmlFor="">Motivo</label>
                    <input {...register('motivo')}  className='w-full bg-zinc-700 text-white px-4 py-2 my-2 rounded-md'   type="text" placeholder='Motivo' />

                </div>

                <div>

                    <label htmlFor="">Fecha</label>
                    <input {...register('fecha')}  className='w-full bg-zinc-700 text-white px-4 py-2 my-2 rounded-md'   type="date" placeholder='Motivo' lang='es-ES' />

                </div>

                <div>

                    <label htmlFor="">Cantidad</label>
                    <input  {...register('cantidad')}  className='w-full bg-zinc-700 text-white px-4 py-2 my-2 rounded-md'   type="text" placeholder='Gasto' />

                </div>

                <div>

                {/* <label htmlFor="">Usuario</label> */}
                <input  {...register('usuario')}  className='hidden w-full bg-zinc-700 text-white px-4 py-2 my-2 rounded-md'   type="text" placeholder='usuario' />

                </div>

    

            </div>
                <button className='bg-red-700 hover:bg-red-600 px-3 py-3 my-5 rounded-md w-full transition duration-700 ease-in-out' >Guardar</button>
        </form>

        <form key={2} onSubmit={onSubmit2}>

        <h2 className='text-3xl py-3'>Busqueda</h2>
       <hr className='py-3' />
       <label htmlFor="">Fecha inicial</label>
       <input className='w-full bg-zinc-700 text-white px-4 py-2 my-3 rounded-lg ' type="date" placeholder='buscar vehiculo' {...register2('inicial')}   />
       <label htmlFor="">Fecha final</label>
       <input className='w-full bg-zinc-700 text-white px-4 py-2 my-3 rounded-lg ' type="date" placeholder='buscar vehiculo' {...register2('final')}   />

       <button type="submit" className='w-full bg-sky-600 rounded-xl my-2 py-2 font-bold text-base' 
    //    onClick={onGasto} 
       >Buscar</button>
        </form>

        {/*
      */
      }
      {/* <h3>Total de gastos: {sumatoria.suma}</h3> */}

        {/* table */}
        <div className='relative overflow-x-auto shadow-md sm:rounded-lg'>

<table  className= 'w-full text-sm text-left text-white dark:text-white '
// className= ' bg-zinc-800 max-w-md w-full p-10 rounded-lg    min-w-full text-center text-md font-light  '
>
    <thead className='text-xs text-white uppercase bg-gray-50 dark:bg-red-700 dark:text-white-400 dark:text-white-400'
    // className='border-b bg-red-900 font-medium dark:border-neutral-500 dark:text-white'
    >
    <tr>
      <th 
      scope="col" className='px-6 py-3'
      // className='col-md-1'
      >#</th>

      <th scope="col" className='px-6 py-3'>Motivo</th>
      <th scope="col" className='px-6 py-3'>Fecha</th>
      <th scope="col" className='px-6 py-3'>Cantidad</th>
      <th scope="col" className='px-6 py-3'>Total</th>
      {/* <th scope="col" className='px-6 py-3'>Cliente</th>
      <th scope="col" className='px-6 py-3'>Color</th>
      <th scope="col" className='px-6 py-3'>Costo</th>
      <th scope="col" className='px-6 py-3'>Venta</th> */}
      {/* <th className='col-md-1'>Serie</th>
      <th className='col-md-1'>Placas</th> 
    <th className='col-md-1'>Inventario</th> */}
      <th> Accion</th>
      {/* <th> Accion</th> */}
      {/* <th>Reporte</th> */}
    </tr>

    </thead>
    <tbody>

         {
            Gastos.map((gasto,i)=>(

                <TableGastos gasto={gasto} i={i} key={gasto.id} totald={anidado+=gasto.cantidad} />
              //  setfech={gasto.}

            ))
        //  tasks.map((task,i)=>  (

        //         // <TaskTable task={task} i={i} key={task.id} />
        //     ) )
            }
            {
                // Gastos.reducer(function(tot,arr){
                //     console.log( tot+arr.cantidad)
                // },0)
            }

    </tbody>

</table>

    </div>



        {/* new */}
        <Toaster richColors   position="top-center" expand={true} />
    </div>
  )
}
