import React, { useEffect } from 'react'
import { useForm } from 'react-hook-form'
import { TaskTable } from '../components/TaskTable'
import { useAuth } from '../context/AuthContext'
import { useTasks } from '../context/TaskContext'

const TaskPage = () => {
    const {user}= useAuth()
    const {getTasks,tasks,getTaskPlaca}=useTasks()

    const {register,handleSubmit,setValue}=useForm()

    const onSubmit = handleSubmit(data =>{
        if (!data.folio==null || !data.folio =='') {
            
            console.log(data);
            getTaskPlaca(data.folio)
        }
        
       return;

    })

    useEffect(() => {
        getTasks()
     
    }, [])

    if(tasks.length ===0) return (<h2>No hay registros</h2>)
    
  return (
    <div className='m-5 bg-zinc-900 p-5 rounded-lg'>
        <form onSubmit={onSubmit}>
          <h2 className='text-5xl text-center'>Lista de vehiculos</h2>
          <hr className='py-2' />

        <input className='w-full bg-zinc-700 text-white px-4 py-2 my-3 rounded-lg ' type="text" placeholder='buscar vehiculo' {...register('folio')} autoFocus  />
        <button className='w-full bg-sky-600 rounded-xl my-2 py-2 font-bold text-base'>Buscar</button>
        </form>
        <div className='relative overflow-x-auto shadow-md sm:rounded-lg'>

        <table  className= 'w-full text-sm text-left text-white dark:text-white '
        // className= ' bg-zinc-800 max-w-md w-full p-10 rounded-lg    min-w-full text-center text-md font-light  '
        >
            <thead className='text-xs text-white uppercase bg-gray-50 dark:bg-red-700 dark:text-white-400 dark:text-white-400'
            // className='border-b bg-red-900 font-medium dark:border-neutral-500 dark:text-white'
            >
            <tr>
              <th 
              scope="col" className='px-6 py-3'
              // className='col-md-1'
              >#</th>

              <th scope="col" className='px-6 py-3'>Vehiculo</th>
              <th scope="col" className='px-6 py-3'>Folio</th>
              <th scope="col" className='px-6 py-3'>Cliente</th>
              <th scope="col" className='px-6 py-3'>Modelo</th>
              <th scope="col" className='px-6 py-3'>Color</th>
              <th scope="col" className='px-6 py-3'>Costo</th>
              <th scope="col" className='px-6 py-3'>Venta</th>
              {/* <th className='col-md-1'>Serie</th>
              <th className='col-md-1'>Placas</th>
            <th className='col-md-1'>Inventario</th> */}
              <th> Editar</th>
              <th> Accion</th>
              {/* <th>Reporte</th> */}
            </tr>

            </thead>
            <tbody>

                 {
                 tasks.map((task,i)=>  (

                        <TaskTable task={task} i={i} key={task.id} />
                    ) )}

            </tbody>

        </table>

            </div>
    </div>
  )
}

export default TaskPage