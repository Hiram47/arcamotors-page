import { Navigate, Outlet } from "react-router-dom"
import { useAuth } from "./context/AuthContext"


const ProtectedRoute = () => {
    const {user,loading,isAuthenticaded} = useAuth()
    console.log(loading,isAuthenticaded,user);

    if (loading) return <h2>...Loading</h2>

    if(!loading  && !isAuthenticaded) return <Navigate to='/login' replace />
    console.log('Protectedroute');

  return (
    <Outlet/>
  )
}

export default ProtectedRoute