import { createContext, useContext, useEffect, useState } from "react";
import { createTicketRequest,getTaxByDayRequest,getNextMonthRequest, deleteTicketrequest, getPuestoRequest, getReporteAllVehiculesRequest, getReporteByvehiculoRequest, getRowToPayRequest, getTicketRequest, updatePaymentrequest,getReportByCarRequest,creategastosRequest, getGastosMonthlyRequest, deleteGastorequest, getGastosFechaRequest, getTotalGastosRequest } from "../api/ticket";


const TicketContext= createContext();

export const useTicket=()=>{
    const context = useContext(TicketContext);

    if (!context) {
        throw new Error('useticket must be used within a Taskprovider')        
    }

    return context;

    
}


export function TicketProvider({children}){
    
    const [tikets, settikets] = useState([])
    const [car, setcar] = useState([])
    const [puestoPolicia, setpuestoPolicia] = useState({})
    const [cars, setcars] = useState([])
    const [datarow, setdatarow] = useState({})
    const [nextmonth, setnextmonth] = useState({})
    const [tax, settax] = useState({})
    const [historyCar, sethistoryCar] = useState([])
    const [Gastos, setGastos] = useState([])
    const [sumatoria, setsumatoria] = useState({})
    const prueba=()=>{
        console.log('hola');
    }

   
    const getTickets=async (id)=>{
        try {
          const {data}=await  getTicketRequest(id)
            console.log(data);
            settikets(data)
            
        } catch (error) {
            console.log(error);
            
        }

    }

    //new 
    const getRowTicket=async (id)=>{
        try {
          const {data}=await  getRowToPayRequest(id)
            console.log(data);
            setdatarow(data)
            // settikets(data)
            
        } catch (error) {
            console.log(error);
            
        }

    }

    const updatePay= async (pagos,id)=>{
        try {
            console.log(pagos);
            const {data}=await updatePaymentrequest(pagos,id)
            console.log(data);
            
        } catch (error) {
            console.log(error);
            
        }
    }
    //
    

    const createTiketGas= async(data)=>{
        try {
            
           let res= await createTicketRequest(data)
           console.log(res);
        } catch (error) {
            console.log(error);
            
        }

    }

    const deleteTicketGas= async(id)=>{
        try {

            let res= await deleteTicketrequest(id)
            console.log(res.data);
            if(res.data==='ticket was deleted') settikets(tikets.filter(ticket => ticket.id != id))
            
        } catch (error) {

            console.log(error);
            
        }
    }

    const getVehiculobyReporte = async(ticket)=>{
        try {
            let {data} =await getReporteByvehiculoRequest(ticket)
            console.log(data);
            setcar(data)

        } catch (error) {
            console.log(error);
            
        }
    }

    const getPuestoReporte = async()=>{
        try {
            let {data}= await getPuestoRequest()
            console.log(data);
            setpuestoPolicia(data)
            
        } catch (error) {
            console.log(error);
            
        }
    }

    const getGastosMonthly = async()=>{
        try {
            let {data}= await getGastosMonthlyRequest()
            console.log(data);
          
            setGastos(data)
            
        } catch (error) {
            console.log(error);
            
        }
    }

    const getCarrosReporte = async(ticket)=>{
        try {
            let {data} = await getReporteAllVehiculesRequest(ticket)
            console.log(data);
            setcars(data)
            
        } catch (error) {
            console.log(error);            
        }
    }

    //new context month
    const getmonthContext = async(inicial,pagos)=>{

        try {
            let {data}= await getNextMonthRequest(inicial,pagos)
            console.log(data.NEXT_MONTH);
            setnextmonth(data.NEXT_MONTH)
            
        } catch (error) {
            console.log(error);
            
        }

    }
    const getReporteCar=async(inicial)=>{
        try {
            let {data}= await getReportByCarRequest(inicial)
            console.log(inicial);
            console.log(data);
            //me quede aqui...
            sethistoryCar(data)
            
        } catch (error) {
            console.log(error);
            
        }
    }

    const getTaxByDay = async(inicial)=>{
        try {
            let {data} = await getTaxByDayRequest(inicial)
            console.log(data);
            settax(data)
            
        } catch (error) {
            console.log(error);
            
        }
    }


    // new gastos
    const createGastosContabilidad= async(data)=>{
        try {
            
           let res= await creategastosRequest(data)
           console.log(res);
        } catch (error) {
            console.log(error);
            
        }

    }

    const deleteGastos= async(id)=>{
        try {

            let res= await deleteGastorequest(id)
            console.log(res.data);
            if(res.data==='gasto was deleted') setGastos(Gastos.filter(gasto => gasto.id != id))
            
        } catch (error) {

            console.log(error);
            
        }
    }

    const getGastosReporte = async(gasto)=>{
        try {
            // aqui me quede 
            console.log(gasto);
            let {data} = await getGastosFechaRequest(gasto)
            console.log(data);
            setGastos(data)
            
        } catch (error) {
            console.log(error);            
        }
    }


    const getTotalGastos = async()=>{
        try {
            let {data}= await getTotalGastosRequest()
            console.log(data);
            setsumatoria(data)
            
        } catch (error) {
            console.log(error);
            
        }
    }




    return(
        <TicketContext.Provider value={{
            prueba,
            getTickets,
            tikets,           
            createTiketGas,
            deleteTicketGas,
            getVehiculobyReporte,
            car,
            getPuestoReporte,
            puestoPolicia,
            getCarrosReporte,
            cars,
            datarow,
            getRowTicket,
            updatePay,
            getmonthContext,
            nextmonth,
            getTaxByDay,
            tax,
            getReporteCar,
            historyCar,
            createGastosContabilidad,
            Gastos,
            getGastosMonthly,
            deleteGastos,
            getGastosReporte,
            getTotalGastos,
            sumatoria
        }}>
            {children}
        </TicketContext.Provider>
    )
}