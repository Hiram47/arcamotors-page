import { useContext, useState } from "react";
import { createContext } from "react";
import { createTaskRequest, getTasksRequest ,getTaskRequest, updateTaskRequest, getTaskPlacaRequest} from "../api/tasks";

const TaskContext= createContext();

export const useTasks=()=>{
    const context = useContext(TaskContext);

    if (!context) {
        throw new Error('usetasks must be used within a Taskprovider')        
    }

    return context;

}

export function TaskProvider({children}){
    const [tasks, settasks] = useState([])

    const getTasks= async()=>{
        try {
            const {data} = await getTasksRequest()
            settasks(data)
            console.log(data);
            
        } catch (error) {
            console.log(error);
            
        }
    }

    const getTask= async(id) =>{
        try {
            let {data} = await getTaskRequest(id)
            console.log(data);
            return data
            settasks(data)

            
        } catch (error) {
            console.log(error);
            
        }
    }

    const getTaskPlaca= async(id)=>{
        try {
            let {data} = await getTaskPlacaRequest(id)
            console.log(data);
            //prueba
            settasks(data)
            return data
            
        } catch (error) {
            console.log(error);
            
        }
    }

    const createTask = async (task) =>{
        try {
            
            console.log(task);
           const res= await createTaskRequest(task)
           console.log(res);
        } catch (error) {
            console.log(error);
            
        }
    }

    const updateTask = async (id,task) =>{
        try {
            
            console.log(id,task);
            const res= await updateTaskRequest(id,task)
            console.log(res);
        } catch (error) {
            console.log(error);
            
        }
    }

    return (
        <TaskContext.Provider value={{tasks,createTask,getTasks,getTask,updateTask,getTaskPlaca}} >
            {children}
        </TaskContext.Provider>
    )
}