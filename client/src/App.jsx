import {BrowserRouter,Routes,Route} from 'react-router-dom'
import { Navbar } from './components/Navbar'
import { AuthProvider } from './context/AuthContext'
import { TaskProvider } from './context/TaskContext'
import { TicketProvider } from './context/TicketContext'
import HomePage from './pages/HomePage'
import LoginPage from './pages/LoginPage'
import ProfilePage from './pages/ProfilePage'
import RegisterPage from './pages/RegisterPage'
import TaskFormPage from './pages/TaskFormPage'
import TaskPage from './pages/TaskPage'
import { TicketFormPage } from './pages/TicketFormPage'
import ProtectedRoute from './ProtectedRoute'
import { ReportePage } from './pages/ReportePage'
import { AllReportsPage } from './pages/AllReportsPage'
import { CotizarPage } from './pages/CotizarPage'
import PagarPage from './pages/PagarPage'
import { Gastos } from './pages/Gastos'

const App = () => {
  return (
    <AuthProvider>
      <TaskProvider>
        <TicketProvider>

          <BrowserRouter>
         <main className='container mx-auto px-10'>
         <Navbar/>
          <Routes>
                <Route path='/' element={<HomePage/>}/>
                <Route path='/login' element={<LoginPage/>}/>
                <Route path='/register' element={<RegisterPage/>}/>

              <Route element={<ProtectedRoute/>}>

              <Route path='/cotizar' element={<CotizarPage/>}/>

              <Route path='/mensualidad/:id' element={<PagarPage/>}/>
              

                <Route path='/vehiculo' element={<TaskPage/>}/>
                <Route path='/add-vehiculo' element={<TaskFormPage/>}/>
                <Route path='/vehiculo/:id' element={<TaskFormPage/>}/>
                <Route path='/pago/:id' element={<TicketFormPage/>} />

                <Route path='/reporte/:id' element={<ReportePage/>} />
                <Route path='/reports' element={<AllReportsPage/>}/>

                <Route path='/profile' element={<ProfilePage/>}/>
                <Route path='/gastos' element={<Gastos/>} />
                
              </Route>

          </Routes>
         </main>
        </BrowserRouter>
        </TicketProvider>
      </TaskProvider>
    </AuthProvider>
  )
}

export default App