-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 25-09-2023 a las 20:26:22
-- Versión del servidor: 10.4.22-MariaDB
-- Versión de PHP: 7.4.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `arcamotors`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pagos`
--

CREATE TABLE `pagos` (
  `id` bigint(20) NOT NULL,
  `id_vehiculo` bigint(20) NOT NULL,
  `venta2` float DEFAULT NULL,
  `usuario_modifico` varchar(150) COLLATE utf8_bin NOT NULL,
  `enganche` float NOT NULL,
  `meses` int(11) NOT NULL DEFAULT 0,
  `interes` int(11) NOT NULL,
  `mensualidad` float NOT NULL,
  `pagos` int(11) NOT NULL DEFAULT 0,
  `fecha_registro` varchar(150) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `pagos`
--

INSERT INTO `pagos` (`id`, `id_vehiculo`, `venta2`, `usuario_modifico`, `enganche`, `meses`, `interes`, `mensualidad`, `pagos`, `fecha_registro`) VALUES
(2, 1, 2587860, 'arca@gmail.com', 45000, 48, 14, 88576.2, 1, '2023-09-19 09:22:18'),
(5, 3, 659000, 'arca@gmail.com', 340000, 30, 14, 15099.3, 30, '2023-09-13 12:44:59'),
(11, 3, 319000, 'arca@gmail.com', 25000, 48, 16, 10829, 0, '9/11/2023'),
(12, 3, 294000, 'arca@gmail.com', 100000, 30, 16, 9570.67, 15, '2023-09-14 15:39:42'),
(15, 4, 680000, 'arca@gmail.com', 45000, 24, 16, 36618.3, 0, '9/12/2023'),
(16, 4, 635000, 'arca@gmail.com', 15000, 48, 16, 22836.7, 0, '9/12/2023'),
(17, 4, 620000, 'arca@gmail.com', 434444, 24, 16, 10700.4, 0, '9/12/2023'),
(29, 5, 179000, 'arca@gmail.com', 45000, 48, 14, 4667.67, 30, '2023-09-18 12:43:48'),
(30, 1, 2489880, 'arca@gmail.com', 120000, 48, 16, 87290.7, 0, '9/19/2023');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `id` int(11) NOT NULL,
  `username` varchar(200) COLLATE utf8_bin NOT NULL,
  `email` varchar(250) COLLATE utf8_bin NOT NULL,
  `password` varchar(255) COLLATE utf8_bin NOT NULL,
  `rol` tinyint(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`id`, `username`, `email`, `password`, `rol`) VALUES
(1, 'ssp', 'ssp@gmail.com', '$2a$10$PEGlHLc2o4XktHaxMg25p.HAS9uhfuxsqT4K8wTXDeTRiMZpMS6.i', 1),
(2, 'arcamotors', 'arca@gmail.com', '$2a$10$5iBNHI9kZ9jjaEEd8cJhreq0x.8WjgMiLK32bILl978.EWFj.BR2e', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `vehiculos`
--

CREATE TABLE `vehiculos` (
  `id` bigint(20) NOT NULL,
  `id_usuario` varchar(150) COLLATE utf8_bin NOT NULL,
  `folio` varchar(100) COLLATE utf8_bin NOT NULL,
  `vehiculo` varchar(150) COLLATE utf8_bin NOT NULL,
  `color` varchar(50) COLLATE utf8_bin NOT NULL,
  `modelo` int(11) NOT NULL,
  `cliente` varchar(200) COLLATE utf8_bin DEFAULT NULL,
  `costo` int(11) DEFAULT NULL,
  `venta` int(11) DEFAULT NULL,
  `enganche` int(11) DEFAULT NULL,
  `enganche_porcentaje` int(11) DEFAULT NULL,
  `meses` int(11) DEFAULT NULL,
  `interes` int(11) DEFAULT NULL,
  `mensualidad` float DEFAULT NULL,
  `pagos` int(11) DEFAULT NULL,
  `usuario_modifico` varchar(200) COLLATE utf8_bin NOT NULL,
  `status` tinyint(1) DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `vehiculos`
--

INSERT INTO `vehiculos` (`id`, `id_usuario`, `folio`, `vehiculo`, `color`, `modelo`, `cliente`, `costo`, `venta`, `enganche`, `enganche_porcentaje`, `meses`, `interes`, `mensualidad`, `pagos`, `usuario_modifico`, `status`) VALUES
(1, 'arca@gmail.com', '0001-2023', 'audi r8', 'rojo', 2016, 'jon snow', 256652, 2587858, 0, NULL, 0, 0, 0, NULL, 'arca@gmail.com', 1),
(2, 'arca@gmail.com', '0002-2023', 'bentley', 'negro', 2020, 'walter white', 3500000, 42000000, NULL, NULL, NULL, NULL, NULL, NULL, 'arca@gmail.com', 1),
(3, 'arca@gmail.com', '0003-2023', 'corvette', 'red', 2020, NULL, 520000, 659000, NULL, NULL, NULL, NULL, NULL, NULL, '', 1),
(4, 'arca@gmail.com', '0004-2023', 'mercedez', 'black', 2022, NULL, 450000, 680000, NULL, NULL, NULL, NULL, NULL, NULL, '', 1),
(5, 'arca@gmail.com', '0006-2023', 'JETTA MK6', 'ROJO', 2015, NULL, 155000, 179000, NULL, NULL, NULL, NULL, NULL, NULL, 'arca@gmail.com', 1);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `pagos`
--
ALTER TABLE `pagos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `vehiculos`
--
ALTER TABLE `vehiculos`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `pagos`
--
ALTER TABLE `pagos`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `vehiculos`
--
ALTER TABLE `vehiculos`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
